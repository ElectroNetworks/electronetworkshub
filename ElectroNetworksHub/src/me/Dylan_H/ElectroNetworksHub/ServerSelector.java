package me.Dylan_H.ElectroNetworksHub;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ServerSelector implements Listener{

	Main plugin;
	
	public ServerSelector(Main instance11) {
		this.plugin = instance11; 
	}
	public void ServerSelectorGUI(Player player) {
		Inventory ServerSelectorGUI = Bukkit.createInventory(null, 27, ChatColor.YELLOW + "" + ChatColor.BOLD + "Servers");
		// Items
		ItemStack Hubs = new ItemStack(Material.GOLDEN_APPLE);
		ItemMeta HubsMeta = Hubs.getItemMeta();
		
		ItemStack WizardFactions = new ItemStack(Material.BLAZE_ROD);
		ItemMeta WizardFactionsMeta = WizardFactions.getItemMeta();
		
		// Display Names
		HubsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Hubs");
		HubsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to list all our hub servers."));
		Hubs.setItemMeta(HubsMeta);
		
		WizardFactionsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Wizard Factions");
		WizardFactionsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to connect to Wizard Factions."));
		WizardFactions.setItemMeta(WizardFactionsMeta);
		
		// Setting The Items
		ServerSelectorGUI.setItem(13, Hubs);
		ServerSelectorGUI.setItem(11, WizardFactions);
		
		player.openInventory(ServerSelectorGUI);
	}
	public void ServerSelectorHubsGUI(Player player) {
		Inventory ServerSelectorHubsGUI = Bukkit.createInventory(null, 9, ChatColor.YELLOW + "" + ChatColor.BOLD + "Servers - Hubs");
		// Items
		ItemStack Hub1 = new ItemStack(Material.NETHER_STAR);
		ItemMeta Hub1Meta = Hub1.getItemMeta();
		
		// Display Names
		Hub1Meta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Hub 1");
		Hub1Meta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to connect to Hub 1."));
		Hub1.setItemMeta(Hub1Meta);
		
		// Setting The Items
		ServerSelectorHubsGUI.setItem(0, Hub1);
		
		player.openInventory(ServerSelectorHubsGUI);
	}
	@EventHandler
	public void ServerSelectorInteract(PlayerInteractEvent e) {
		if((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
			ItemStack currentItem = e.getPlayer().getItemInHand();
			if((currentItem == null) || (currentItem.getType() == Material.AIR)) {
				return;
			}
			if((!currentItem.hasItemMeta()) || ((currentItem.hasItemMeta()) && (!currentItem.getItemMeta().hasDisplayName()))) {
				return;
			}
			if(currentItem.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "" + ChatColor.BOLD + "Servers")) {
				Player player = (Player) e.getPlayer();
				ServerSelectorGUI(player);
			}
		}
	}
	@EventHandler
	public void ServerSelectorInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		if(!e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Servers")) return;
		if(e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Servers")) {
			e.setCancelled(true);
			if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Hubs")) {
				ServerSelectorHubsGUI(player);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Wizard Factions")) {
				player.sendMessage(prefix + ChatColor.BLUE + "Now attempting to connect you to Wizard Factions...");
				this.plugin.sendToServer(player, "WizardFactions");
			}
		}
	}
	@EventHandler
	public void ServerSelectorHubsInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		if(!e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Servers - Hubs")) return;
		if(e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Servers - Hubs")) {
			e.setCancelled(true);
			if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Hub 1")) {
				player.sendMessage(prefix + ChatColor.BLUE + "Now attempting to connect you to Hub-1...");
				this.plugin.sendToServer(player, "Hub-1");
			}
		}
	}
}