package me.Dylan_H.ElectroNetworksHub;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class PlayerPing implements Listener{

	Main plugin;
	
	public PlayerPing(Main instance7) {
		this.plugin = instance7;
	}
	@EventHandler
	public void MOTD(ServerListPingEvent e) {
		e.setMotd(ChatColor.GOLD + "Server Name: " + ChatColor.YELLOW + this.plugin.getConfig().getString("ServerName") + "\n" + ChatColor.GOLD + "Server Mode: " + ChatColor.YELLOW + this.plugin.getConfig().getString("ServerMode").replaceAll("_", " "));
	}
}