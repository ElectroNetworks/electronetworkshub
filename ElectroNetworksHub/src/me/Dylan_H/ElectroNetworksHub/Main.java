package me.Dylan_H.ElectroNetworksHub;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Main extends JavaPlugin implements Listener{
	
	File file;
	FileConfiguration electrodatabase;
	Scoreboard sidebar;
	
	@SuppressWarnings("deprecation")
	public void onEnable() {
		getLogger().info("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
		getLogger().info("ELECTRO NETWORKS HUB NOW ENABLED!");
		getLogger().info("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
		getCommand("gm").setExecutor(new Commands(this));
		getCommand("gamemode").setExecutor(new Commands(this));
		getCommand("weather").setExecutor(new Commands(this));
		getCommand("time").setExecutor(new Commands(this));
		getCommand("rules").setExecutor(new Commands(this));
		getCommand("core").setExecutor(new Commands(this));
		getCommand("spawn").setExecutor(new Commands(this));
		getCommand("setspawn").setExecutor(new Commands(this));
		getCommand("tp").setExecutor(new Commands(this));
		getCommand("electros").setExecutor(new Commands(this));
		getCommand("shout").setExecutor(new Commands(this));
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new OnJoin(this), this);
		pm.registerEvents(new OnQuit(this), this);
		pm.registerEvents(new Config(this), this);
		pm.registerEvents(new PlayerSettings(this), this);
		pm.registerEvents(new PlayerWardrobe(this), this);
		pm.registerEvents(new PlayerPing(this), this);
		pm.registerEvents(new DoubleJump(this), this);
		pm.registerEvents(new OnDrop(this), this);
		pm.registerEvents(new ServerSelector(this), this);
		pm.registerEvents(new FireworkThrower(this), this);
		pm.registerEvents(this, this);
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		FileConfiguration config = getConfig();
		config.options().copyDefaults(true);
		saveConfig();
		file = new File(getDataFolder(), "electrodatabase.yml");
		electrodatabase = YamlConfiguration.loadConfiguration(file);
		electrodatabase.options().copyDefaults(true);
		Bukkit.getServer().getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
			public void run() {
				for(Player player : Bukkit.getOnlinePlayers()) {
					if(getConfig().getString("Players." + player.getUniqueId() + ".Particle").equals("Flame")) {
						player.getWorld().playEffect(player.getLocation(), Effect.MOBSPAWNER_FLAMES, 2004);
					}else if(getConfig().getString("Players." + player.getUniqueId() + ".Particle").equals("EnderSignal")) {
						player.getWorld().playEffect(player.getLocation(), Effect.ENDER_SIGNAL, 2004);
					}else if(getConfig().getString("Players." + player.getUniqueId() + ".Particle").equals("Heart")) {
						player.getWorld().playEffect(player.getLocation().add(0, 2, 0), Effect.HEART, 2004);
					}
				}
			}
		}, 20, 10);
		Bukkit.getServer().getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
			public void run() {
				for(Player player : Bukkit.getOnlinePlayers()) {
					if(getConfig().getString("Players." + player.getUniqueId() + ".Particle").equals("Footstep")) {
						player.getWorld().playEffect(player.getLocation(), Effect.FOOTSTEP, 2004);
					}else if(getConfig().getString("Players." + player.getUniqueId() + ".Particle").equals("Slime")) {
						player.getWorld().playEffect(player.getLocation().add(0, 2, 0), Effect.SLIME, 2004);
					}else if(getConfig().getString("Players." + player.getUniqueId() + ".Particle").equals("Critical")) {
						player.getWorld().playEffect(player.getLocation().add(0, 1, 0), Effect.CRIT, 2004);
					}else if(getConfig().getString("Players." + player.getUniqueId() + ".Particle").equals("LavaDrip")) {
						player.getWorld().playEffect(player.getLocation().add(0, 2, 0), Effect.LAVADRIP, 2004);
					}else if(getConfig().getString("Players." + player.getUniqueId() + ".Particle").equals("WaterDrip")) {
						player.getWorld().playEffect(player.getLocation().add(0, 2, 0), Effect.WATERDRIP, 2004);
					}else if(getConfig().getString("Players." + player.getUniqueId() + ".Particle").equals("NetherPortal")) {
						player.getWorld().playEffect(player.getLocation().add(0, 1, 0), Effect.PORTAL, 10000);
					}
				}
			}
		}, 20, 5);
	}
	public void saveelectrodatabase() {
		try {
			electrodatabase.save(file);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void onDisable() {
		getLogger().info("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
		getLogger().info("ELECTRO NETWORKS HUB NOW DISABLED!");
		getLogger().info("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
	}
	public void sendToServer(Player player, String targetServer) {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(b);
		try {
			out.writeUTF("Connect");
			out.writeUTF(targetServer);
		}catch (Exception e) {
			e.printStackTrace();
		}
		player.sendPluginMessage(this, "BungeeCord", b.toByteArray());
	}
	@EventHandler
	public void SidebarSetter(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		PermissionUser user = PermissionsEx.getUser(player);
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		this.sidebar = manager.getNewScoreboard();
		
		Objective objective = this.sidebar.registerNewObjective("EN", "EN");
		objective.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Electro Networks");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		Score score12 = objective.getScore(ChatColor.BLUE + " ");
		score12.setScore(12);
		
		Score score10 = objective.getScore(ChatColor.GOLD + "" + ChatColor.BOLD + "Server:");
		score10.setScore(10);
		
		Score score9 = objective.getScore(ChatColor.YELLOW + "" + ChatColor.BOLD + getConfig().getString("ServerName"));
		score9.setScore(9);
		
		Score score8 = objective.getScore(ChatColor.AQUA + " ");
		score8.setScore(8);
		
		Score score7 = objective.getScore(ChatColor.GOLD + "" + ChatColor.BOLD + "Name:");
		score7.setScore(7);
		
		Score score6 = objective.getScore(ChatColor.YELLOW + "" + ChatColor.BOLD + player.getDisplayName());
		score6.setScore(6);
		
		Score score5 = objective.getScore(ChatColor.BLACK + " ");
		score5.setScore(5);
		
		Score score4 = objective.getScore(ChatColor.GOLD + "" + ChatColor.BOLD + "Rank:");
		score4.setScore(4);
		
		Score score3 = objective.getScore(ChatColor.YELLOW + "" + ChatColor.BOLD + user.getPrefix().replaceAll("&", "�"));
		score3.setScore(3);
		
		player.setScoreboard(sidebar);
	}
} 