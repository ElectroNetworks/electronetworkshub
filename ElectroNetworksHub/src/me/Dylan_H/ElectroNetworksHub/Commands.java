package me.Dylan_H.ElectroNetworksHub;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.inventivegames.util.tellraw.TellrawConverterLite;
import de.inventivegames.util.title.TitleManager;

public class Commands implements CommandExecutor{

	Main plugin;
	
	public Commands(Main instance1) {
		this.plugin = instance1;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		final Player player = (Player) sender;
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		if(sender instanceof Player) {
			if(cmd.getName().equalsIgnoreCase("gm") || (cmd.getName().equalsIgnoreCase("gamemode"))) {
				if(player.hasPermission("electronetworks.gamemode")) {
					if(args.length == 0) {
						player.sendMessage(prefix + ChatColor.RED + "Please specify a gamemode to switch to.");
					}else if(args[0].equalsIgnoreCase("c") || (args[0].equalsIgnoreCase("creative"))) {
						player.setGameMode(GameMode.CREATIVE);
						player.sendMessage(prefix + ChatColor.YELLOW + "Your gamemode has been set to: " + ChatColor.GOLD + "Creative");
					}else if(args[0].equalsIgnoreCase("s") || (args[0].equalsIgnoreCase("survival"))) {
						player.setGameMode(GameMode.SURVIVAL);
						player.sendMessage(prefix + ChatColor.YELLOW + "Your gamemode has been set to: " + ChatColor.GOLD + "Survival");
					}else if(args[0].equalsIgnoreCase("a") || (args[0].equalsIgnoreCase("adventure"))) {
						player.setGameMode(GameMode.ADVENTURE);
						player.sendMessage(prefix + ChatColor.YELLOW + "Your gamemode has been set to: " + ChatColor.GOLD + "Adventure");
					}else if(args[0].equalsIgnoreCase("spectate") || (args[0].equalsIgnoreCase("spectator"))) {
						player.setGameMode(GameMode.SPECTATOR);
						player.sendMessage(prefix + ChatColor.YELLOW + "Your gamemode has been set to: " + ChatColor.GOLD + "Spectate");
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, that isn't a valid gamemode.");
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "Sorry, you don't have permission to perform this command.");
				}
			}else if(cmd.getName().equalsIgnoreCase("weather")) {
				if(player.hasPermission("electronetworks.weather")) {
					if(args.length == 0) {
						player.sendMessage(prefix + ChatColor.RED + "Please specify a type of weather.");
					}else if(args[0].equalsIgnoreCase("sun")) {
						player.getWorld().setStorm(false);
						player.sendMessage(prefix + ChatColor.YELLOW + "You have set the weather to: " + ChatColor.GOLD + "Sun");
					}else if(args[0].equalsIgnoreCase("rain")) {
						player.getWorld().setStorm(true);
						player.sendMessage(prefix + ChatColor.YELLOW + "You have set the weather to: " + ChatColor.GOLD + "Rain");
					}else if(args[0].equalsIgnoreCase("thunder")) {
						player.getWorld().setStorm(true);
						player.getWorld().setThundering(true);
						player.sendMessage(prefix + ChatColor.YELLOW + "You have set the weather to: " + ChatColor.GOLD + "Thunder");
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, that isn't a valid weather type.");
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "Sorry, you don't have permission to perform this command.");
				}
			}else if(cmd.getName().equalsIgnoreCase("time")) {
				if(player.hasPermission("electronetworks.time")) {
					if(args.length == 0) {
						player.sendMessage(prefix + ChatColor.RED + "Please specify day or night.");
					}else if(args[0].equalsIgnoreCase("day")) {
						player.getWorld().setTime(5000);
					}else if(args[0].equalsIgnoreCase("night")) {
						player.getWorld().setTime(1000000);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, that isn't a valid time.");
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "Sorry, you don't have permission to perform this command.");
				}
			}else if(cmd.getName().equalsIgnoreCase("core")) {
				if(player.hasPermission("electronetworks.core")) {
					if(args.length == 0) {
						player.sendMessage(prefix + ChatColor.RED + "Please specify the core command you want to use.");
					}else if(args[0].equalsIgnoreCase("reload")) {
						this.plugin.reloadConfig();
						player.sendMessage(prefix + ChatColor.BLUE + "Successfully reloaded the Electro Networks Hub configuration file.");
					}else if(args[0].equalsIgnoreCase("mode")) {
						if(args.length == 1) {
							player.sendMessage(prefix + ChatColor.RED + "Please specify a server mode.");
						}else if(args[1].equalsIgnoreCase("development")) {
							this.plugin.getConfig().set("ServerMode", "Development_Mode");;
							player.sendMessage(prefix + ChatColor.YELLOW + "Set the server mode to: " + ChatColor.GOLD + "Development Mode");
						}else if(args[1].equalsIgnoreCase("normal")) {
							this.plugin.getConfig().set("ServerMode", "Normal_Mode");
							player.sendMessage(prefix + ChatColor.YELLOW + "Set the server mode to: " + ChatColor.GOLD + "Normal Mode");
						}else if(args[1].equalsIgnoreCase("build")) {
							this.plugin.getConfig().set("ServerMode", "Build_Mode");
							player.sendMessage(prefix + ChatColor.YELLOW + "Set the server mode to: " + ChatColor.GOLD + "Build Mode");
						}else{
							player.sendMessage(prefix + ChatColor.RED + "Please specify a valid server mode.");
						}
					}else if(args[0].equalsIgnoreCase("restart")) {
						player.sendMessage(prefix + ChatColor.BLUE + "Restart process initiated.");
						final String restartTitle = TellrawConverterLite.convertToJSON("�4Restart");
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String time10 = TellrawConverterLite.convertToJSON("�cThe server will be restarting in: 10 seconds.");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, time10);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 0);
								}
							}
						}, 0);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String time9 = TellrawConverterLite.convertToJSON("�cThe server will be restarting in: 9 seconds.");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, time9);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 0);
								}
							}
						}, 30);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String time8 = TellrawConverterLite.convertToJSON("�cThe server will be restarting in: 8 seconds.");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, time8);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 0);
								}
							}
						}, 40);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String time7 = TellrawConverterLite.convertToJSON("�cThe server will be restarting in: 7 seconds.");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, time7);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 0);
								}
							}
						}, 60);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String time6 = TellrawConverterLite.convertToJSON("�cThe server will be restarting in: 6 seconds.");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, time6);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 0);
								}
							}
						}, 80);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String time5 = TellrawConverterLite.convertToJSON("�cThe server will be restarting in: 5 seconds.");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, time5);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 0);
								}
							}
						}, 100);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String time4 = TellrawConverterLite.convertToJSON("�cThe server will be restarting in: 4 seconds.");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, time4);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 0);
								}
							}
						}, 130);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String time3 = TellrawConverterLite.convertToJSON("�cThe server will be restarting in: 3 seconds.");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, time3);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 1);
								}
							}
						}, 140);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String time2 = TellrawConverterLite.convertToJSON("�cThe server will be restarting in: 2 seconds.");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, time2);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 1);
								}
							}
						}, 160);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String time1 = TellrawConverterLite.convertToJSON("�cThe server will be restarting in: 1 seconds.");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, time1);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 1);
								}
							}
						}, 180);
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							public void run() {
								for(Player onlinePlayers : Bukkit.getOnlinePlayers()) {
									String timeRestart = TellrawConverterLite.convertToJSON("�cThe server will be restarting now!");
									TitleManager.sendTimings(onlinePlayers, 0, 30, 0);
									TitleManager.sendTitle(onlinePlayers, restartTitle);
									TitleManager.sendSubTitle(onlinePlayers, timeRestart);
									onlinePlayers.playSound(onlinePlayers.getLocation(), Sound.NOTE_PLING, 1, 2);
									Bukkit.shutdown();
								}
							}
						}, 200);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "Sorry, you don't have permission to perform this command.");
				}
			}else if(cmd.getName().equalsIgnoreCase("setspawn")) {
				if(player.hasPermission("electronetworks.setspawn")) {
					this.plugin.getConfig().set("SpawnPoint.X", player.getLocation().getBlockX());
					this.plugin.getConfig().set("SpawnPoint.Y", player.getLocation().getBlockY());
					this.plugin.getConfig().set("SpawnPoint.Z", player.getLocation().getBlockZ());
					this.plugin.saveConfig();
					player.getWorld().setSpawnLocation(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ());
					player.sendMessage(prefix + ChatColor.YELLOW + "Successfully set the server spawn point.");
				}else{
					player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have permission to perform this command.");
				}
			}else if(cmd.getName().equalsIgnoreCase("spawn")) {
				if(player.hasPermission("electronetworks.spawn")) {
					Location Spawn = new Location(player.getWorld(), this.plugin.getConfig().getInt("SpawnPoint.X"), this.plugin.getConfig().getInt("SpawnPoint.Y"), this.plugin.getConfig().getInt("SpawnPoint.Z"));
					player.teleport(Spawn);
				}else{
					player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have permission to perform this command.");
				}
			}else if(cmd.getName().equalsIgnoreCase("tp")) {
				if(player.hasPermission("electronetworks.teleport")) {
					if(args.length == 0) {
						player.sendMessage(prefix + ChatColor.RED + "Please specify a player to teleport.");
					}else if(args.length == 1) {
						try {
							Player targetPlayer1 = player.getServer().getPlayer(args[0]);
							Location targetPlayer1Location = targetPlayer1.getLocation();
							player.teleport(targetPlayer1Location);
							player.sendMessage(prefix + ChatColor.YELLOW + "Teleport commencing...");
						}catch (Exception e){
							player.sendMessage(prefix + ChatColor.RED + "The player you specified is not online.");
						}
					}else if(args.length == 2) {
						try {
							Player targetPlayer1 = player.getServer().getPlayer(args[0]);
							Player targetPlayer2 = player.getServer().getPlayer(args[1]);
							Location targetPlayer2Location = targetPlayer2.getLocation();
							targetPlayer1.teleport(targetPlayer2Location);
							player.sendMessage(prefix + ChatColor.YELLOW + "Teleport commencing...");
						}catch (Exception e) {
							player.sendMessage(prefix + ChatColor.RED + "Once of the players you specified are not online.");
						}
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have permission to perform this command.");
				}
			}else if(cmd.getName().equalsIgnoreCase("rules")) {
				player.sendMessage(prefix + ChatColor.YELLOW + "Now listing rules...");
				player.sendMessage(ChatColor.GOLD + "1 - No advertising.");
				player.sendMessage(ChatColor.GOLD + "2 - No swearing.");
				player.sendMessage(ChatColor.GOLD + "3 - Respect all staff and players.");
				player.sendMessage(ChatColor.GOLD + "4 - Don't exploit bugs and glitches.");
				player.sendMessage(ChatColor.GOLD + "5 - No spamming of any kind.");
				player.sendMessage(ChatColor.GOLD + "6 - No hacked clients.");
				player.sendMessage(ChatColor.BLUE + "We have rules because we want our server to be a fun place to play.");
				player.sendMessage(ChatColor.RED + "Failure to comply with these rules will result in a punishment. Donating does not exempt you from the rules!");
			}else if(cmd.getName().equalsIgnoreCase("electros")){
					if(args.length == 0) {
						player.sendMessage(prefix + ChatColor.YELLOW + "Now listing commands for the electros currency system:");
						player.sendMessage(ChatColor.GOLD + "/electros add USER AMOUNT");
						player.sendMessage(ChatColor.GOLD + "/electros remove USER AMOUNT");
						player.sendMessage(ChatColor.GOLD + "/electros view USER");
						player.sendMessage(ChatColor.GOLD + "/electros transfer USER AMOUNT");
					}else if(args[0].equalsIgnoreCase("add")) {
						if(player.hasPermission("electronetworks.electros.manage")) {
							try {
								Player target = Bukkit.getServer().getPlayer(args[1]);
								int amount = Integer.parseInt(args[2]);
								this.plugin.getConfig().set("Electros." + target.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + target.getUniqueId() + ".Balance") + amount));
								this.plugin.saveConfig();
								player.sendMessage(prefix + ChatColor.YELLOW + "Successfully added " + ChatColor.GOLD + args[2] + ChatColor.YELLOW + " electros to the user: " + ChatColor.GOLD + target.getName());
							}catch (Exception e) {
								player.sendMessage(prefix + ChatColor.RED + "Unable to add electros to that user as he/she is offline.");
							}
						}
					}else if(args[0].equalsIgnoreCase("remove")) {
						if(player.hasPermission("electronetworks.electros.manage")) {
							try {
								Player target = Bukkit.getServer().getPlayer(args[1]);
								int amount = Integer.parseInt(args[2]);
								if(amount > this.plugin.getConfig().getInt("Electros." + target.getUniqueId() + ".Balance")) {
									player.sendMessage(prefix + ChatColor.RED + "You cannot remove that amount of electros from the user's account as his/her electros will go below 0.");
								}else{
									this.plugin.getConfig().set("Electros." + target.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + target.getUniqueId() + ".Balance") - amount));
									this.plugin.saveConfig();
									player.sendMessage(prefix + ChatColor.YELLOW + "Successfully removed " + ChatColor.GOLD + args[2] + ChatColor.YELLOW + " electros from the user: " + ChatColor.GOLD + target.getName());
								}
							}catch (Exception e) {
								player.sendMessage(prefix + ChatColor.RED + "Unable to remove electros from that user as he/she is offline.");
							}
						}
					}else if(args[0].equalsIgnoreCase("view")) {
						if(player.hasPermission("electronetworks.electros.manage")) {
							try {
								Player target = Bukkit.getServer().getPlayer(args[1]);
								int amount = this.plugin.getConfig().getInt("Electros." + target.getUniqueId() + ".Balance");
								player.sendMessage(prefix + ChatColor.YELLOW + "The player " + ChatColor.GOLD + target.getName() + ChatColor.YELLOW + " has " + ChatColor.GOLD + amount + ChatColor.YELLOW + " electros.");
							}catch (Exception e) {
								player.sendMessage(prefix + ChatColor.RED + "Unable to view the amount of electros that user has as he/she is offline.");
							}
						}
					}else if(args[0].equalsIgnoreCase("transfer")) {
						if(player.hasPermission("electronetworks.electros.transfer")) {
							try {
								Player target = Bukkit.getServer().getPlayer(args[1]);
								int amount = Integer.parseInt(args[2]);
								if(amount > this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance")) {
									player.sendMessage(prefix + ChatColor.RED + "You cannot transfer that amount of electros to that user's account as your electros will go below 0.");
								}else{
									this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - amount));
									this.plugin.getConfig().set("Electros." + target.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + target.getUniqueId() + ".Balance") + amount));
									player.sendMessage(prefix + ChatColor.YELLOW + "You successfully sent " + ChatColor.GOLD + amount + ChatColor.YELLOW + " electros to the player: " + ChatColor.GOLD + target.getName());
									target.sendMessage(prefix + ChatColor.YELLOW + "You received " + ChatColor.GOLD + amount + ChatColor.YELLOW + " electros from the player: " + ChatColor.GOLD + player.getName());
								}
							}catch (Exception e) {
								player.sendMessage(prefix + ChatColor.RED + "Unable to transfer electros to that user as he/she is offline.");
							}
						}
					}
			}else if(cmd.getName().equalsIgnoreCase("shout")) {
				if(player.hasPermission("electronetworks.staff")) {
					if(args.length == 0) {
						player.sendMessage(prefix + ChatColor.RED + "Please specify a message to shout to the server.");
					}else if(args.length >= 1) {
						StringBuilder sb = new StringBuilder();
						for(int i = 0; i < args.length; i++)sb.append(args[i]+" ");
						Bukkit.broadcastMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "SHOUT" + ChatColor.GRAY + " || " + ChatColor.GOLD + player.getName() + ": " + ChatColor.YELLOW + sb);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "Sorry, you don't have permission to perform this command.");
				}
			}
		}
		return false;
	}
}