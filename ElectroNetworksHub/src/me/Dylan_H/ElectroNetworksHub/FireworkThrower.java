package me.Dylan_H.ElectroNetworksHub;

import java.lang.reflect.Field;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworkThrower implements Listener{

	Main plugin;
	
	public FireworkThrower(Main instance12) {
		this.plugin = instance12; 
	}
	@EventHandler
	public void FireworkThrowerInteract(PlayerInteractEvent e) {
		if((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
			ItemStack currentItem = e.getPlayer().getItemInHand();
			if((currentItem == null) || (currentItem.getType() == Material.AIR)) {
				return;
			}
			if((!currentItem.hasItemMeta()) || ((currentItem.hasItemMeta()) && (!currentItem.getItemMeta().hasDisplayName()))) {
				return;
			}
			if(currentItem.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "" + ChatColor.BOLD + "Firework Thrower")) {
				Player player = (Player) e.getPlayer();
				Snowball snowball = player.launchProjectile(Snowball.class);
				snowball.setShooter(player);
				player.getWorld().playEffect(player.getLocation(), Effect.LARGE_SMOKE, 2004);
			}
		}
	}
	@EventHandler
	public void LaunchFirework(ProjectileHitEvent e) {
		if(e.getEntity() instanceof Snowball) {
			if(e.getEntity().getShooter() instanceof Player) {
				World world = e.getEntity().getWorld();
				Location loc = e.getEntity().getLocation();
				Firework firework = world.spawn(loc, Firework.class);
				FireworkMeta fm = firework.getFireworkMeta();
				FireworkEffect effect = FireworkEffect.builder()
						.flicker(false)
						.trail(false)
						.with(Type.BALL)
						.with(Type.BALL_LARGE)
						.withColor(Color.YELLOW)
						.withColor(Color.ORANGE)
						.withFade(Color.GRAY)
						.build();
				fm.clearEffects();
				fm.addEffect(effect);
				Field field;
				try {
					field = fm.getClass().getDeclaredField("power");
					field.setAccessible(true);
					field.set(fm, -1);
				} catch (NoSuchFieldException e1) {
					e1.printStackTrace();
				} catch (SecurityException e1) {
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				}
				firework.setFireworkMeta(fm);
			}
		}
	}
}