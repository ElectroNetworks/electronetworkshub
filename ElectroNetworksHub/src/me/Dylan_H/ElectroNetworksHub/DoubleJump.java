package me.Dylan_H.ElectroNetworksHub;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class DoubleJump implements Listener {

	Main plugin;
	
	public DoubleJump(Main instance8) {
		this.plugin = instance8;
	}
	@EventHandler
	public void ToggleFlight(PlayerToggleFlightEvent e) {
		Player player = e.getPlayer();
		if(player.getGameMode() == GameMode.CREATIVE || player.getGameMode() == GameMode.SPECTATOR || this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Flight") == true) return;
		e.setCancelled(true);
		player.setAllowFlight(false);
		player.setFlying(false);
		player.setVelocity(player.getLocation().getDirection().multiply(2.0).setY(1));
		Location loc = player.getLocation();
		player.playSound(loc, Sound.ENDERDRAGON_WINGS, 10, 5);
	}
	@EventHandler
	public void PlayerMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		if((player.getGameMode() != GameMode.CREATIVE) && (player.getLocation().subtract(0, 1, 0).getBlock().getType() != Material.AIR) && (!player.isFlying())) {
			player.setAllowFlight(true);
		}
	}
}