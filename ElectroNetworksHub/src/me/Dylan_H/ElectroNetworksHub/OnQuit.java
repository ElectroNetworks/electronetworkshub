package me.Dylan_H.ElectroNetworksHub;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class OnQuit implements Listener{

	Main plugin;
	
	public OnQuit(Main instance3) {
		this.plugin = instance3;
	}
	@EventHandler
	public void PlayerQuitAnnouncements(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		if(player.hasPermission("electronetworks.staff")) {
			e.setQuitMessage(prefix + ChatColor.GOLD + "Staff Member: " + player.getName() + ChatColor.YELLOW + " has left the game.");
		}else{
			e.setQuitMessage(prefix + ChatColor.GRAY + player.getName() + " has left the game.");
		}
	}
}