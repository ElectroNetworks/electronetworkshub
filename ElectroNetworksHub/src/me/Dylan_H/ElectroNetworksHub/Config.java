package me.Dylan_H.ElectroNetworksHub;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Config implements Listener{

	Main plugin;
	
	public Config(Main instance4) {
		this.plugin = instance4;
	}
	@EventHandler
	public void ConfigSetter(PlayerJoinEvent e) {
		this.plugin.getConfig().set("Players." + e.getPlayer().getUniqueId() + ".Username", e.getPlayer().getName());
		this.plugin.saveConfig();
		if(!this.plugin.getConfig().contains("PlayerJoinMessages.Server")) { // Server Name
			this.plugin.getConfig().addDefault("PlayerJoinMessages.Server", "InsertNameHere");
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("PlayerJoinMessages.News")) { // Server News
			this.plugin.getConfig().addDefault("PlayerJoinMessages.News", "InsertNewsHere");
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("SpawnPoint.X")) {
			this.plugin.getConfig().addDefault("SpawnPoint.X", "1");
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("SpawnPoint.Y")) {
			this.plugin.getConfig().addDefault("SpawnPoint.Y", "1");
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("SpawnPoint.Z")) {
			this.plugin.getConfig().addDefault("SpawnPoint.Z", "1");
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("ServerMode")) {
			this.plugin.getConfig().addDefault("ServerMode", "Normal");
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("ServerName")) {
			this.plugin.getConfig().addDefault("ServerName", "InsertNameHere");
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("Players." + e.getPlayer().getUniqueId() + ".Speed")) { // Player Settings (Speed)
			this.plugin.getConfig().addDefault("Players." + e.getPlayer().getUniqueId() + ".Speed", false);
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("Players." + e.getPlayer().getUniqueId() + ".Jump")) { // Player Settings (Jump)
			this.plugin.getConfig().addDefault("Players." + e.getPlayer().getUniqueId() + ".Jump", false);
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("Players." + e.getPlayer().getUniqueId() + ".Chat")) { // Player Settings (Chat)
			this.plugin.getConfig().addDefault("Players." + e.getPlayer().getUniqueId() + ".Chat", true);
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("Players." + e.getPlayer().getUniqueId() + ".NightVision")) { // Player Settings (Night Vision)
			this.plugin.getConfig().addDefault("Players." + e.getPlayer().getUniqueId() + ".NightVision", false);
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("Players." + e.getPlayer().getUniqueId() + ".Flight")) { // Player Settings (Flight)
			this.plugin.getConfig().addDefault("Players." + e.getPlayer().getUniqueId() + ".Flight", false);
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("Players." + e.getPlayer().getUniqueId() + ".Username")) { // Player Username
			this.plugin.getConfig().addDefault("Players." + e.getPlayer().getUniqueId() + ".Username", e.getPlayer().getName());
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("Players." + e.getPlayer().getUniqueId() + ".JoinSound")) {
			this.plugin.getConfig().addDefault("Players." + e.getPlayer().getUniqueId() + ".JoinSound", "None");
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("Players." + e.getPlayer().getUniqueId() + ".Particle")) {
			this.plugin.getConfig().addDefault("Players." + e.getPlayer().getUniqueId() + ".Particle", "None");
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("Electros." + e.getPlayer().getUniqueId() + ".Balance")) {
			this.plugin.getConfig().addDefault("Electros." + e.getPlayer().getUniqueId() + ".Balance", 500);
			this.plugin.saveConfig();
		}if(!this.plugin.getConfig().contains("Electros." + e.getPlayer().getUniqueId() + ".Username")) {
			this.plugin.getConfig().addDefault("Electros." + e.getPlayer().getUniqueId() + ".Username", e.getPlayer().getName());
			this.plugin.saveConfig();
		}
	}
}