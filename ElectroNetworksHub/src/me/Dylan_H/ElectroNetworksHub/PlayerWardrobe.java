package me.Dylan_H.ElectroNetworksHub;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class PlayerWardrobe implements Listener{

	Main plugin;
	
	public PlayerWardrobe(Main instance6) {
		this.plugin = instance6; 
	}
	// MAIN GUI
	public void PlayerWardrobeGUI(Player player) {
		Inventory PlayerWardrobeGUI = Bukkit.createInventory(null, 45, ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe");
		// Items
		ItemStack Clothes = new ItemStack(Material.LEATHER_HELMET);
		ItemMeta ClothesMeta = Clothes.getItemMeta();
		
		ItemStack Hats = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		ItemMeta HatsMeta = Hats.getItemMeta();
		
		ItemStack Armor = new ItemStack(Material.CHAINMAIL_HELMET);
		ItemMeta ArmorMeta = Armor.getItemMeta();
		
		ItemStack Gadgets = new ItemStack(Material.REDSTONE);
		ItemMeta GadgetsMeta = Gadgets.getItemMeta();
		
		ItemStack BannerHats = new ItemStack(Material.BANNER, 1, (byte) 11);
		ItemMeta BannerHatsMeta = BannerHats.getItemMeta();
		
		ItemStack Particles = new ItemStack(Material.FIREWORK);
		ItemMeta ParticlesMeta = Particles.getItemMeta();
		
		ItemStack JoinSounds = new ItemStack(Material.NOTE_BLOCK);
		ItemMeta JoinSoundsMeta = JoinSounds.getItemMeta();
		
		ItemStack Morphs = new ItemStack(Material.SKULL_ITEM);
		ItemMeta MorphsMeta = Morphs.getItemMeta();
		
		// Display Names
		ClothesMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Clothes");
		ClothesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here select some jazzy clothes to wear."));
		Clothes.setItemMeta(ClothesMeta);
		
		HatsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Hats");
		HatsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to select a hat to wear."));
		Hats.setItemMeta(HatsMeta);
		
		ArmorMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Armor");
		ArmorMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to select some defensive armor to wear."));
		Armor.setItemMeta(ArmorMeta);
		
		GadgetsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Gadgets");
		GadgetsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to select a fun gadget to use."));
		Gadgets.setItemMeta(GadgetsMeta);
		
		BannerHatsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Banner Hats");
		BannerHatsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to select a banner hat to show off",ChatColor.GOLD + "  to everyone on the server."));
		BannerHats.setItemMeta(BannerHatsMeta);
		
		ParticlesMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Particles");
		ParticlesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to select a particle to make you stand out."));
		Particles.setItemMeta(ParticlesMeta);
		
		JoinSoundsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Join Sounds");
		JoinSoundsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to select a join sound which will be",ChatColor.GOLD + "  heard by everyone when you join."));
		JoinSounds.setItemMeta(JoinSoundsMeta);
		
		MorphsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Morphs");
		MorphsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to turn in to a mob and trick people."));
		Morphs.setItemMeta(MorphsMeta);
		
		// Setting The Items
		PlayerWardrobeGUI.setItem(1, Clothes);
		PlayerWardrobeGUI.setItem(4, Hats);
		PlayerWardrobeGUI.setItem(7, Armor);
		PlayerWardrobeGUI.setItem(18, Gadgets);
		PlayerWardrobeGUI.setItem(22, BannerHats);
		PlayerWardrobeGUI.setItem(26, Particles);
		PlayerWardrobeGUI.setItem(38, JoinSounds);
		PlayerWardrobeGUI.setItem(42, Morphs);
		
		player.openInventory(PlayerWardrobeGUI);
	}
	// CLOTHES GUI
	public void PlayerWardrobeClothesGUI(Player player) {
		Inventory PlayerWardrobeClothesGUI = Bukkit.createInventory(null, 45, ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Clothes");
		// Items
		ItemStack REDHat = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta REDHatMeta = (LeatherArmorMeta) REDHat.getItemMeta();
		REDHatMeta.setColor(Color.RED);
		
		ItemStack REDShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta REDShirtMeta = (LeatherArmorMeta) REDShirt.getItemMeta();
		REDShirtMeta.setColor(Color.RED);
		
		ItemStack REDTrousers = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta REDTrousersMeta = (LeatherArmorMeta) REDTrousers.getItemMeta();
		REDTrousersMeta.setColor(Color.RED);
		
		ItemStack REDShoes = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta REDShoesMeta = (LeatherArmorMeta) REDShoes.getItemMeta();
		REDShoesMeta.setColor(Color.RED);
		
		ItemStack BLUEHat = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta BLUEHatMeta = (LeatherArmorMeta) BLUEHat.getItemMeta();
		BLUEHatMeta.setColor(Color.BLUE);
		
		ItemStack BLUEShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta BLUEShirtMeta = (LeatherArmorMeta) BLUEShirt.getItemMeta();
		BLUEShirtMeta.setColor(Color.BLUE);
		
		ItemStack BLUETrousers = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta BLUETrousersMeta = (LeatherArmorMeta) BLUETrousers.getItemMeta();
		BLUETrousersMeta.setColor(Color.BLUE);
		
		ItemStack BLUEShoes = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta BLUEShoesMeta = (LeatherArmorMeta) BLUEShoes.getItemMeta();
		BLUEShoesMeta.setColor(Color.BLUE);
		
		ItemStack GREENHat = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta GREENHatMeta = (LeatherArmorMeta) GREENHat.getItemMeta();
		GREENHatMeta.setColor(Color.GREEN);
		
		ItemStack GREENShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta GREENShirtMeta = (LeatherArmorMeta) GREENShirt.getItemMeta();
		GREENShirtMeta.setColor(Color.GREEN);
		
		ItemStack GREENTrousers = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta GREENTrousersMeta = (LeatherArmorMeta) GREENTrousers.getItemMeta();
		GREENTrousersMeta.setColor(Color.GREEN);
		
		ItemStack GREENShoes = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta GREENShoesMeta = (LeatherArmorMeta) GREENShoes.getItemMeta();
		GREENShoesMeta.setColor(Color.GREEN);
		
		ItemStack YELLOWHat = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta YELLOWHatMeta = (LeatherArmorMeta) YELLOWHat.getItemMeta();
		YELLOWHatMeta.setColor(Color.YELLOW);
		
		ItemStack YELLOWShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta YELLOWShirtMeta = (LeatherArmorMeta) YELLOWShirt.getItemMeta();
		YELLOWShirtMeta.setColor(Color.YELLOW);
		
		ItemStack YELLOWTrousers = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta YELLOWTrousersMeta = (LeatherArmorMeta) YELLOWTrousers.getItemMeta();
		YELLOWTrousersMeta.setColor(Color.YELLOW);
		
		ItemStack YELLOWShoes = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta YELLOWShoesMeta = (LeatherArmorMeta) YELLOWShoes.getItemMeta();
		YELLOWShoesMeta.setColor(Color.YELLOW);
		
		ItemStack PURPLEHat = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta PURPLEHatMeta = (LeatherArmorMeta) PURPLEHat.getItemMeta();
		PURPLEHatMeta.setColor(Color.PURPLE);
		
		ItemStack PURPLEShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta PURPLEShirtMeta = (LeatherArmorMeta) PURPLEShirt.getItemMeta();
		PURPLEShirtMeta.setColor(Color.PURPLE);
		
		ItemStack PURPLETrousers = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta PURPLETrousersMeta = (LeatherArmorMeta) PURPLETrousers.getItemMeta();
		PURPLETrousersMeta.setColor(Color.PURPLE);
		
		ItemStack PURPLEShoes = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta PURPLEShoesMeta = (LeatherArmorMeta) PURPLEShoes.getItemMeta();
		PURPLEShoesMeta.setColor(Color.PURPLE);
		
		ItemStack ORANGEHat = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta ORANGEHatMeta = (LeatherArmorMeta) ORANGEHat.getItemMeta();
		ORANGEHatMeta.setColor(Color.ORANGE);
		
		ItemStack ORANGEShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta ORANGEShirtMeta = (LeatherArmorMeta) ORANGEShirt.getItemMeta();
		ORANGEShirtMeta.setColor(Color.ORANGE);
		
		ItemStack ORANGETrousers = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta ORANGETrousersMeta = (LeatherArmorMeta) ORANGETrousers.getItemMeta();
		ORANGETrousersMeta.setColor(Color.ORANGE);
		
		ItemStack ORANGEShoes = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta ORANGEShoesMeta = (LeatherArmorMeta) ORANGEShoes.getItemMeta();
		ORANGEShoesMeta.setColor(Color.ORANGE);
		
		ItemStack WHITEHat = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta WHITEHatMeta = (LeatherArmorMeta) WHITEHat.getItemMeta();
		WHITEHatMeta.setColor(Color.WHITE);
		
		ItemStack WHITEShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta WHITEShirtMeta = (LeatherArmorMeta) WHITEShirt.getItemMeta();
		WHITEShirtMeta.setColor(Color.WHITE);
		
		ItemStack WHITETrousers = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta WHITETrousersMeta = (LeatherArmorMeta) WHITETrousers.getItemMeta();
		WHITETrousersMeta.setColor(Color.WHITE);
		
		ItemStack WHITEShoes = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta WHITEShoesMeta = (LeatherArmorMeta) WHITEShoes.getItemMeta();
		WHITEShoesMeta.setColor(Color.WHITE);
		
		ItemStack LIMEHat = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta LIMEHatMeta = (LeatherArmorMeta) LIMEHat.getItemMeta();
		LIMEHatMeta.setColor(Color.LIME);
		
		ItemStack LIMEShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta LIMEShirtMeta = (LeatherArmorMeta) LIMEShirt.getItemMeta();
		LIMEShirtMeta.setColor(Color.LIME);
		
		ItemStack LIMETrousers = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta LIMETrousersMeta = (LeatherArmorMeta) LIMETrousers.getItemMeta();
		LIMETrousersMeta.setColor(Color.LIME);
		
		ItemStack LIMEShoes = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta LIMEShoesMeta = (LeatherArmorMeta) LIMEShoes.getItemMeta();
		LIMEShoesMeta.setColor(Color.LIME);
		
		ItemStack FUCHSIAHat = new ItemStack(Material.LEATHER_HELMET);
		LeatherArmorMeta FUCHSIAHatMeta = (LeatherArmorMeta) FUCHSIAHat.getItemMeta();
		FUCHSIAHatMeta.setColor(Color.FUCHSIA);
		
		ItemStack FUCHSIAShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
		LeatherArmorMeta FUCHSIAShirtMeta = (LeatherArmorMeta) FUCHSIAShirt.getItemMeta();
		FUCHSIAShirtMeta.setColor(Color.FUCHSIA);
		
		ItemStack FUCHSIATrousers = new ItemStack(Material.LEATHER_LEGGINGS);
		LeatherArmorMeta FUCHSIATrousersMeta = (LeatherArmorMeta) FUCHSIATrousers.getItemMeta();
		FUCHSIATrousersMeta.setColor(Color.FUCHSIA);
		
		ItemStack FUCHSIAShoes = new ItemStack(Material.LEATHER_BOOTS);
		LeatherArmorMeta FUCHSIAShoesMeta = (LeatherArmorMeta) FUCHSIAShoes.getItemMeta();
		FUCHSIAShoesMeta.setColor(Color.FUCHSIA);
		
		ItemStack MainMenu = new ItemStack(Material.PAINTING);
		ItemMeta MainMenuMeta = MainMenu.getItemMeta();
		
		ItemStack ResetClothes = new ItemStack(Material.TNT);
		ItemMeta ResetClothesMeta = ResetClothes.getItemMeta();
		
		// Display Names
		REDHatMeta.setDisplayName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Red Hat");
		REDHatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		REDHat.setItemMeta(REDHatMeta);
		
		REDShirtMeta.setDisplayName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Red Shirt");
		REDShirtMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		REDShirt.setItemMeta(REDShirtMeta);
		
		REDTrousersMeta.setDisplayName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Red Trousers");
		REDTrousersMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		REDTrousers.setItemMeta(REDTrousersMeta);
		
		REDShoesMeta.setDisplayName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Red Shoes");
		REDShoesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		REDShoes.setItemMeta(REDShoesMeta);
		
		BLUEHatMeta.setDisplayName(ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Blue Hat");
		BLUEHatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		BLUEHat.setItemMeta(BLUEHatMeta);
		
		BLUEShirtMeta.setDisplayName(ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Blue Shirt");
		BLUEShirtMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		BLUEShirt.setItemMeta(BLUEShirtMeta);
		
		BLUETrousersMeta.setDisplayName(ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Blue Trousers");
		BLUETrousersMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		BLUETrousers.setItemMeta(BLUETrousersMeta);
		
		BLUEShoesMeta.setDisplayName(ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Blue Shoes");
		BLUEShoesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		BLUEShoes.setItemMeta(BLUEShoesMeta);
		
		GREENHatMeta.setDisplayName(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Green Hat");
		GREENHatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		GREENHat.setItemMeta(GREENHatMeta);
		
		GREENShirtMeta.setDisplayName(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Green Shirt");
		GREENShirtMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		GREENShirt.setItemMeta(GREENShirtMeta);
		
		GREENTrousersMeta.setDisplayName(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Green Trousers");
		GREENTrousersMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		GREENTrousers.setItemMeta(GREENTrousersMeta);
		
		GREENShoesMeta.setDisplayName(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Green Shoes");
		GREENShoesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		GREENShoes.setItemMeta(GREENShoesMeta);
		
		YELLOWHatMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Yellow Hat");
		YELLOWHatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		YELLOWHat.setItemMeta(YELLOWHatMeta);
		
		YELLOWShirtMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Yellow Shirt");
		YELLOWShirtMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		YELLOWShirt.setItemMeta(YELLOWShirtMeta);
		
		YELLOWTrousersMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Yellow Trousers");
		YELLOWTrousersMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		YELLOWTrousers.setItemMeta(YELLOWTrousersMeta);
		
		YELLOWShoesMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Yellow Shoes");
		YELLOWShoesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		YELLOWShoes.setItemMeta(YELLOWShoesMeta);
		
		PURPLEHatMeta.setDisplayName(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Purple Hat");
		PURPLEHatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		PURPLEHat.setItemMeta(PURPLEHatMeta);
		
		PURPLEShirtMeta.setDisplayName(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Purple Shirt");
		PURPLEShirtMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		PURPLEShirt.setItemMeta(PURPLEShirtMeta);
		
		PURPLETrousersMeta.setDisplayName(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Purple Trousers");
		PURPLETrousersMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		PURPLETrousers.setItemMeta(PURPLETrousersMeta);
		
		PURPLEShoesMeta.setDisplayName(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Purple Shoes");
		PURPLEShoesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		PURPLEShoes.setItemMeta(PURPLEShoesMeta);
		
		ORANGEHatMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Orange Hat");
		ORANGEHatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		ORANGEHat.setItemMeta(ORANGEHatMeta);
		
		ORANGEShirtMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Orange Shirt");
		ORANGEShirtMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		ORANGEShirt.setItemMeta(ORANGEShirtMeta);
		
		ORANGETrousersMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Orange Trousers");
		ORANGETrousersMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		ORANGETrousers.setItemMeta(ORANGETrousersMeta);
		
		ORANGEShoesMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Orange Shoes");
		ORANGEShoesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		ORANGEShoes.setItemMeta(ORANGEShoesMeta);
		
		WHITEHatMeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + "White Hat");
		WHITEHatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		WHITEHat.setItemMeta(WHITEHatMeta);
		
		WHITEShirtMeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + "White Shirt");
		WHITEShirtMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		WHITEShirt.setItemMeta(WHITEShirtMeta);
		
		WHITETrousersMeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + "White Trousers");
		WHITETrousersMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		WHITETrousers.setItemMeta(WHITETrousersMeta);
		
		WHITEShoesMeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + "White Shoes");
		WHITEShoesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		WHITEShoes.setItemMeta(WHITEShoesMeta);
		
		LIMEHatMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Lime Hat");
		LIMEHatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		LIMEHat.setItemMeta(LIMEHatMeta);
		
		LIMEShirtMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Lime Shirt");
		LIMEShirtMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		LIMEShirt.setItemMeta(LIMEShirtMeta);
		
		LIMETrousersMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Lime Trousers");
		LIMETrousersMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		LIMETrousers.setItemMeta(LIMETrousersMeta);
		
		LIMEShoesMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Lime Shoes");
		LIMEShoesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		LIMEShoes.setItemMeta(LIMEShoesMeta);
		
		FUCHSIAHatMeta.setDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Pink Hat");
		FUCHSIAHatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		FUCHSIAHat.setItemMeta(FUCHSIAHatMeta);
		
		FUCHSIAShirtMeta.setDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Pink Shirt");
		FUCHSIAShirtMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		FUCHSIAShirt.setItemMeta(FUCHSIAShirtMeta);
		
		FUCHSIATrousersMeta.setDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Pink Trousers");
		FUCHSIATrousersMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		FUCHSIATrousers.setItemMeta(FUCHSIATrousersMeta);
		
		FUCHSIAShoesMeta.setDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Pink Shoes");
		FUCHSIAShoesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip."));
		FUCHSIAShoes.setItemMeta(FUCHSIAShoesMeta);
		
		MainMenuMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Main Menu");
		MainMenuMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to return to the main menu."));
		MainMenu.setItemMeta(MainMenuMeta);
		
		ResetClothesMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Reset Clothes");
		ResetClothesMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to reset your clothes."));
		ResetClothes.setItemMeta(ResetClothesMeta);
		
		// Setting The Items
		PlayerWardrobeClothesGUI.setItem(0, REDHat);
		PlayerWardrobeClothesGUI.setItem(1, BLUEHat);
		PlayerWardrobeClothesGUI.setItem(2, GREENHat);
		PlayerWardrobeClothesGUI.setItem(3, YELLOWHat);
		PlayerWardrobeClothesGUI.setItem(4, PURPLEHat);
		PlayerWardrobeClothesGUI.setItem(5, ORANGEHat);
		PlayerWardrobeClothesGUI.setItem(6, WHITEHat);
		PlayerWardrobeClothesGUI.setItem(7, LIMEHat);
		PlayerWardrobeClothesGUI.setItem(8, FUCHSIAHat);
		PlayerWardrobeClothesGUI.setItem(9, REDShirt);
		PlayerWardrobeClothesGUI.setItem(10, BLUEShirt);
		PlayerWardrobeClothesGUI.setItem(11, GREENShirt);
		PlayerWardrobeClothesGUI.setItem(12, YELLOWShirt);
		PlayerWardrobeClothesGUI.setItem(13, PURPLEShirt);
		PlayerWardrobeClothesGUI.setItem(14, ORANGEShirt);
		PlayerWardrobeClothesGUI.setItem(15, WHITEShirt);
		PlayerWardrobeClothesGUI.setItem(16, LIMEShirt);
		PlayerWardrobeClothesGUI.setItem(17, FUCHSIAShirt);
		PlayerWardrobeClothesGUI.setItem(18, REDTrousers);
		PlayerWardrobeClothesGUI.setItem(19, BLUETrousers);
		PlayerWardrobeClothesGUI.setItem(20, GREENTrousers);
		PlayerWardrobeClothesGUI.setItem(21, YELLOWTrousers);
		PlayerWardrobeClothesGUI.setItem(22, PURPLETrousers);
		PlayerWardrobeClothesGUI.setItem(23, ORANGETrousers);
		PlayerWardrobeClothesGUI.setItem(24, WHITETrousers);
		PlayerWardrobeClothesGUI.setItem(25, LIMETrousers);
		PlayerWardrobeClothesGUI.setItem(26, FUCHSIATrousers);
		PlayerWardrobeClothesGUI.setItem(27, REDShoes);
		PlayerWardrobeClothesGUI.setItem(28, BLUEShoes);
		PlayerWardrobeClothesGUI.setItem(29, GREENShoes);
		PlayerWardrobeClothesGUI.setItem(30, YELLOWShoes);
		PlayerWardrobeClothesGUI.setItem(31, PURPLEShoes);
		PlayerWardrobeClothesGUI.setItem(32, ORANGEShoes);
		PlayerWardrobeClothesGUI.setItem(33, WHITEShoes);
		PlayerWardrobeClothesGUI.setItem(34, LIMEShoes);
		PlayerWardrobeClothesGUI.setItem(35, FUCHSIAShoes);
		PlayerWardrobeClothesGUI.setItem(38, ResetClothes);
		PlayerWardrobeClothesGUI.setItem(40, MainMenu);
		PlayerWardrobeClothesGUI.setItem(42, ResetClothes);
		
		player.openInventory(PlayerWardrobeClothesGUI);
	}
	// HATS GUI
	public void PlayerWardrobeHatsGUI(Player player) {
		Inventory PlayerWardrobeHatsGUI = Bukkit.createInventory(null, 27, ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Hats");
		// Items
		ItemStack CraftingTable = new ItemStack(Material.WORKBENCH);
		ItemMeta CraftingTableMeta = CraftingTable.getItemMeta();
		
		ItemStack TNT = new ItemStack(Material.TNT);
		ItemMeta TNTMeta = TNT.getItemMeta();
		
		ItemStack HayBale = new ItemStack(Material.HAY_BLOCK);
		ItemMeta HayBaleMeta = HayBale.getItemMeta();
		
		ItemStack Melon = new ItemStack(Material.MELON_BLOCK);
		ItemMeta MelonMeta = Melon.getItemMeta();
		
		ItemStack RedstoneLamp = new ItemStack(Material.REDSTONE_LAMP_OFF);
		ItemMeta RedstoneLampMeta = RedstoneLamp.getItemMeta();
		
		ItemStack SeaLantern = new ItemStack(Material.SEA_LANTERN);
		ItemMeta SeaLanternMeta = SeaLantern.getItemMeta();
		
		ItemStack PumpkinGlowing = new ItemStack(Material.JACK_O_LANTERN);
		ItemMeta PumpkinGlowingMeta = PumpkinGlowing.getItemMeta();
		
		ItemStack Pumpkin = new ItemStack(Material.PUMPKIN);
		ItemMeta PumpkinMeta = Pumpkin.getItemMeta();
		
		ItemStack SlimeBlock = new ItemStack(Material.SLIME_BLOCK);
		ItemMeta SlimeBlockMeta = SlimeBlock.getItemMeta();
		
		ItemStack Furnace = new ItemStack(Material.FURNACE);
		ItemMeta FurnaceMeta = Furnace.getItemMeta();
		
		ItemStack Dispenser = new ItemStack(Material.DISPENSER);
		ItemMeta DispenserMeta = Dispenser.getItemMeta();
		
		ItemStack Dropper = new ItemStack(Material.DROPPER);
		ItemMeta DropperMeta = Dropper.getItemMeta();
		
		ItemStack NoteBlock = new ItemStack(Material.NOTE_BLOCK);
		ItemMeta NoteBlockMeta = NoteBlock.getItemMeta();
		
		ItemStack Piston = new ItemStack(Material.PISTON_BASE);
		ItemMeta PistonMeta = Piston.getItemMeta();
		
		ItemStack Sponge = new ItemStack(Material.SPONGE);
		ItemMeta SpongeMeta = Sponge.getItemMeta();
		
		ItemStack DiamondBlock = new ItemStack(Material.DIAMOND_BLOCK);
		ItemMeta DiamondBlockMeta = DiamondBlock.getItemMeta();
		
		ItemStack EmeraldBlock = new ItemStack(Material.EMERALD_BLOCK);
		ItemMeta EmeraldBlockMeta = EmeraldBlock.getItemMeta();
		
		ItemStack GoldBlock = new ItemStack(Material.GOLD_BLOCK);
		ItemMeta GoldBlockMeta = GoldBlock.getItemMeta();
		
		ItemStack ResetHat = new ItemStack(Material.TNT);
		ItemMeta ResetHatMeta = ResetHat.getItemMeta();
		
		ItemStack MainMenu = new ItemStack(Material.PAINTING);
		ItemMeta MainMenuMeta = MainMenu.getItemMeta();
		
		// Display Names
		CraftingTableMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Crafting Table Hat");
		CraftingTableMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1000 Electros."));
		CraftingTable.setItemMeta(CraftingTableMeta);
		
		TNTMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "TNT Hat");
		TNTMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		TNT.setItemMeta(TNTMeta);
		
		HayBaleMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Hay Bale Hat");
		HayBaleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1000 Electros."));
		HayBale.setItemMeta(HayBaleMeta);
		
		MelonMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Melon Hat");
		MelonMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1500 Electros."));
		Melon.setItemMeta(MelonMeta);
		
		RedstoneLampMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Redstone Lamp Hat");
		RedstoneLampMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		RedstoneLamp.setItemMeta(RedstoneLampMeta);
		
		SeaLanternMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Sea Lantern Hat");
		SeaLanternMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2500 Electros."));
		SeaLantern.setItemMeta(SeaLanternMeta);
		
		PumpkinGlowingMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Jack O'Lantern Hat");
		PumpkinGlowingMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2500 Electros."));
		PumpkinGlowing.setItemMeta(PumpkinGlowingMeta);
		
		PumpkinMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Pumpkin Hat");
		PumpkinMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1500 Electros."));
		Pumpkin.setItemMeta(PumpkinMeta);
		
		SlimeBlockMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Slime Block Hat");
		SlimeBlockMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		SlimeBlock.setItemMeta(SlimeBlockMeta);
		
		FurnaceMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Furnace Hat");
		FurnaceMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		Furnace.setItemMeta(FurnaceMeta);
		
		DispenserMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Dispenser Hat");
		DispenserMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		Dispenser.setItemMeta(DispenserMeta);
		
		DropperMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Dropper Hat");
		DropperMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		Dropper.setItemMeta(DropperMeta);
		
		NoteBlockMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Note Block Hat");
		NoteBlockMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 3000 Electros."));
		NoteBlock.setItemMeta(NoteBlockMeta);
		
		PistonMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Piston Hat");
		PistonMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 3000 Electros."));
		Piston.setItemMeta(PistonMeta);
		
		SpongeMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Sponge Hat");
		SpongeMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		Sponge.setItemMeta(SpongeMeta);
		
		DiamondBlockMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Diamond Block Hat");
		DiamondBlockMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 3500 Electros."));
		DiamondBlock.setItemMeta(DiamondBlockMeta);
		
		EmeraldBlockMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Emerald Block Hat");
		EmeraldBlockMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		EmeraldBlock.setItemMeta(EmeraldBlockMeta);
		
		GoldBlockMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold Block Hat");
		GoldBlockMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 3000 Electros."));
		GoldBlock.setItemMeta(GoldBlockMeta);
		
		ResetHatMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Reset Hat");
		ResetHatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to reset your hat."));
		ResetHat.setItemMeta(ResetHatMeta);
		
		MainMenuMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Main Menu");
		MainMenuMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to return to the main menu."));
		MainMenu.setItemMeta(MainMenuMeta);
		
		// Setting The Items
		PlayerWardrobeHatsGUI.setItem(0, CraftingTable);
		PlayerWardrobeHatsGUI.setItem(1, TNT);
		PlayerWardrobeHatsGUI.setItem(2, HayBale);
		PlayerWardrobeHatsGUI.setItem(3, Melon);
		PlayerWardrobeHatsGUI.setItem(4, RedstoneLamp);
		PlayerWardrobeHatsGUI.setItem(5, SeaLantern);
		PlayerWardrobeHatsGUI.setItem(6, PumpkinGlowing);
		PlayerWardrobeHatsGUI.setItem(7, Pumpkin);
		PlayerWardrobeHatsGUI.setItem(8, SlimeBlock);
		PlayerWardrobeHatsGUI.setItem(9, Furnace);
		PlayerWardrobeHatsGUI.setItem(10, Dispenser);
		PlayerWardrobeHatsGUI.setItem(11, Dropper);
		PlayerWardrobeHatsGUI.setItem(12, NoteBlock);
		PlayerWardrobeHatsGUI.setItem(13, Piston);
		PlayerWardrobeHatsGUI.setItem(14, Sponge);
		PlayerWardrobeHatsGUI.setItem(15, DiamondBlock);
		PlayerWardrobeHatsGUI.setItem(16, EmeraldBlock);
		PlayerWardrobeHatsGUI.setItem(17, GoldBlock);
		PlayerWardrobeHatsGUI.setItem(20, ResetHat);
		PlayerWardrobeHatsGUI.setItem(22, MainMenu);
		PlayerWardrobeHatsGUI.setItem(24, ResetHat);
		
		player.openInventory(PlayerWardrobeHatsGUI);
	}
	// ARMOR GUI
	public void PlayerWardrobeArmorGUI(Player player) {
		Inventory PlayerWardrobeArmorGUI = Bukkit.createInventory(null, 45, ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Armor");
		// Items
		ItemStack LeatherHelmet = new ItemStack(Material.LEATHER_HELMET);
		ItemMeta LeatherHelmetMeta = LeatherHelmet.getItemMeta();
		
		ItemStack LeatherChestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
		ItemMeta LeatherChestplateMeta = LeatherChestplate.getItemMeta();
		
		ItemStack LeatherLeggings = new ItemStack(Material.LEATHER_LEGGINGS);
		ItemMeta LeatherLeggingsMeta = LeatherLeggings.getItemMeta();
		
		ItemStack LeatherBoots = new ItemStack(Material.LEATHER_BOOTS);
		ItemMeta LeatherBootsMeta = LeatherBoots.getItemMeta();
		
		ItemStack ChainHelmet = new ItemStack(Material.CHAINMAIL_HELMET);
		ItemMeta ChainHelmetMeta = ChainHelmet.getItemMeta();
		
		ItemStack ChainChestplate = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ItemMeta ChainChestplateMeta = ChainChestplate.getItemMeta();
		
		ItemStack ChainLeggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
		ItemMeta ChainLeggingsMeta = ChainLeggings.getItemMeta();
		
		ItemStack ChainBoots = new ItemStack(Material.CHAINMAIL_BOOTS);
		ItemMeta ChainBootsMeta = ChainBoots.getItemMeta();
		
		ItemStack IronHelmet = new ItemStack(Material.IRON_HELMET);
		ItemMeta IronHelmetMeta = IronHelmet.getItemMeta();
		
		ItemStack IronChestplate = new ItemStack(Material.IRON_CHESTPLATE);
		ItemMeta IronChestplateMeta = IronChestplate.getItemMeta();
		
		ItemStack IronLeggings = new ItemStack(Material.IRON_LEGGINGS);
		ItemMeta IronLeggingsMeta = IronLeggings.getItemMeta();
		
		ItemStack IronBoots = new ItemStack(Material.IRON_BOOTS);
		ItemMeta IronBootsMeta = IronBoots.getItemMeta();
		
		ItemStack GoldHelmet = new ItemStack(Material.GOLD_HELMET);
		ItemMeta GoldHelmetMeta = GoldHelmet.getItemMeta();
		
		ItemStack GoldChestplate = new ItemStack(Material.GOLD_CHESTPLATE);
		ItemMeta GoldChestplateMeta = GoldChestplate.getItemMeta();
		
		ItemStack GoldLeggings = new ItemStack(Material.GOLD_LEGGINGS);
		ItemMeta GoldLeggingsMeta = GoldLeggings.getItemMeta();
		
		ItemStack GoldBoots = new ItemStack(Material.GOLD_BOOTS);
		ItemMeta GoldBootsMeta = GoldBoots.getItemMeta();
		
		ItemStack DiamondHelmet = new ItemStack(Material.DIAMOND_HELMET);
		ItemMeta DiamondHelmetMeta = DiamondHelmet.getItemMeta();
		
		ItemStack DiamondChestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta DiamondChestplateMeta = DiamondChestplate.getItemMeta();
		
		ItemStack DiamondLeggings = new ItemStack(Material.DIAMOND_LEGGINGS);
		ItemMeta DiamondLeggingsMeta = DiamondLeggings.getItemMeta();
		
		ItemStack DiamondBoots = new ItemStack(Material.DIAMOND_BOOTS);
		ItemMeta DiamondBootsMeta = DiamondBoots.getItemMeta();
		
		ItemStack ResetArmor = new ItemStack(Material.TNT);
		ItemMeta ResetArmorMeta  = ResetArmor.getItemMeta();
		
		ItemStack MainMenu = new ItemStack(Material.PAINTING);
		ItemMeta MainMenuMeta = MainMenu.getItemMeta();
		
		// Display Names
		LeatherHelmetMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Leather Helmet");
		LeatherHelmetMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 500 Electros."));
		LeatherHelmet.setItemMeta(LeatherHelmetMeta);
		
		LeatherChestplateMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Leather Chestplate");
		LeatherChestplateMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1000 Electros."));
		LeatherChestplate.setItemMeta(LeatherChestplateMeta);
		
		LeatherLeggingsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Leather Leggings");
		LeatherLeggingsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		LeatherLeggings.setItemMeta(LeatherLeggingsMeta);
		
		LeatherBootsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Leather Boots");
		LeatherBootsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 500 Electros."));
		LeatherBoots.setItemMeta(LeatherBootsMeta);
		
		ChainHelmetMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Chain Helmet");
		ChainHelmetMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 600 Electros."));
		ChainHelmet.setItemMeta(ChainHelmetMeta);
		
		ChainChestplateMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Chain Chestplate");
		ChainChestplateMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1100 Electros."));
		ChainChestplate.setItemMeta(ChainChestplateMeta);
		
		ChainLeggingsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Chain Leggings");
		ChainLeggingsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 850 Electros."));
		ChainLeggings.setItemMeta(ChainLeggingsMeta);
		
		ChainBootsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Chain Boots");
		ChainBootsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 600 Electros."));
		ChainBoots.setItemMeta(ChainBootsMeta);
		
		IronHelmetMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Iron Helmet");
		IronHelmetMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		IronHelmet.setItemMeta(IronHelmetMeta);
		
		IronChestplateMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Iron Chestplate");
		IronChestplateMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1250 Electros."));
		IronChestplate.setItemMeta(IronChestplateMeta);
		
		IronLeggingsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Iron Leggings");
		IronLeggingsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1000 Electros."));
		IronLeggings.setItemMeta(IronLeggingsMeta);
		
		IronBootsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Iron Boots");
		IronBootsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		IronBoots.setItemMeta(IronBootsMeta);
		
		GoldHelmetMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold Helmet");
		GoldHelmetMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		GoldHelmet.setItemMeta(GoldHelmetMeta);
		
		GoldChestplateMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold Chestplate");
		GoldChestplateMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		GoldChestplate.setItemMeta(GoldChestplateMeta);
		
		GoldLeggingsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold Leggings");
		GoldLeggingsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		GoldLeggings.setItemMeta(GoldLeggingsMeta);
		
		GoldBootsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold Boots");
		GoldBootsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		GoldBoots.setItemMeta(GoldBootsMeta);
		
		DiamondHelmetMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Diamond Helmet");
		DiamondHelmetMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		DiamondHelmet.setItemMeta(DiamondHelmetMeta);
		
		DiamondChestplateMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Diamond Chestplate");
		DiamondChestplateMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		DiamondChestplate.setItemMeta(DiamondChestplateMeta);
		
		DiamondLeggingsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Diamond Leggings");
		DiamondLeggingsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		DiamondLeggings.setItemMeta(DiamondLeggingsMeta);
		
		DiamondBootsMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Diamond Boots");
		DiamondBootsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		DiamondBoots.setItemMeta(DiamondBootsMeta);
		
		ResetArmorMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Reset Armor");
		ResetArmorMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to reset your armor."));
		ResetArmor.setItemMeta(ResetArmorMeta);
		
		MainMenuMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Main Menu");
		MainMenuMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to return to the main menu."));
		MainMenu.setItemMeta(MainMenuMeta);
		
		// Setting The Items
		PlayerWardrobeArmorGUI.setItem(0, LeatherHelmet);
		PlayerWardrobeArmorGUI.setItem(2, ChainHelmet);
		PlayerWardrobeArmorGUI.setItem(4, IronHelmet);
		PlayerWardrobeArmorGUI.setItem(6, GoldHelmet);
		PlayerWardrobeArmorGUI.setItem(8, DiamondHelmet);
		PlayerWardrobeArmorGUI.setItem(9, LeatherChestplate);
		PlayerWardrobeArmorGUI.setItem(11, ChainChestplate);
		PlayerWardrobeArmorGUI.setItem(13, IronChestplate);
		PlayerWardrobeArmorGUI.setItem(15, GoldChestplate);
		PlayerWardrobeArmorGUI.setItem(17, DiamondChestplate);
		PlayerWardrobeArmorGUI.setItem(18, LeatherLeggings);
		PlayerWardrobeArmorGUI.setItem(20, ChainLeggings);
		PlayerWardrobeArmorGUI.setItem(22, IronLeggings);
		PlayerWardrobeArmorGUI.setItem(24, GoldLeggings);
		PlayerWardrobeArmorGUI.setItem(26, DiamondLeggings);
		PlayerWardrobeArmorGUI.setItem(27, LeatherBoots);
		PlayerWardrobeArmorGUI.setItem(29, ChainBoots);
		PlayerWardrobeArmorGUI.setItem(31, IronBoots);
		PlayerWardrobeArmorGUI.setItem(33, GoldBoots);
		PlayerWardrobeArmorGUI.setItem(35, DiamondBoots);
		PlayerWardrobeArmorGUI.setItem(38, ResetArmor);
		PlayerWardrobeArmorGUI.setItem(40, MainMenu);
		PlayerWardrobeArmorGUI.setItem(42, ResetArmor);
		
		player.openInventory(PlayerWardrobeArmorGUI);
	}
	// JOIN SOUNDS GUI
	public void PlayerWardrobeJoinSoundsGUI(Player player) {
		Inventory PlayerWardrobeJoinSoundsGUI = Bukkit.createInventory(null, 18, ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - J Sounds");
		// Items
		ItemStack WaterDunk = new ItemStack(Material.WATER_BUCKET);
		ItemMeta WaterDunkMeta = WaterDunk.getItemMeta();
		
		ItemStack AnvilDrop = new ItemStack(Material.ANVIL);
		ItemMeta AnvilDropMeta = AnvilDrop.getItemMeta();
		
		ItemStack PistonExtending = new ItemStack(Material.PISTON_BASE);
		ItemMeta PistonExtendingMeta = PistonExtending.getItemMeta();
		
		ItemStack CatMeow = new ItemStack(Material.RAW_FISH);
		ItemMeta CatMeowMeta = CatMeow.getItemMeta();
		
		ItemStack ExplodingTNT = new ItemStack(Material.TNT);
		ItemMeta ExplodingTNTMeta = ExplodingTNT.getItemMeta();
		
		ItemStack ChestOpening = new ItemStack(Material.CHEST);
		ItemMeta ChestOpeningMeta = ChestOpening.getItemMeta();
		
		ItemStack GhastDeath = new ItemStack(Material.QUARTZ);
		ItemMeta GhastDeathMeta = GhastDeath.getItemMeta();
		
		ItemStack Lightning = new ItemStack(Material.STICK);
		ItemMeta LightningMeta = Lightning.getItemMeta();
		
		ItemStack DogBark = new ItemStack(Material.BONE);
		ItemMeta DogBarkMeta = DogBark.getItemMeta();
		
		ItemStack ResetJoinSound = new ItemStack(Material.TNT);
		ItemMeta ResetJoinSoundMeta = ResetJoinSound.getItemMeta();
		
		ItemStack MainMenu = new ItemStack(Material.PAINTING);
		ItemMeta MainMenuMeta = MainMenu.getItemMeta();
		
		// Display Names
		WaterDunkMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Water Dunk Sound");
		WaterDunkMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		WaterDunk.setItemMeta(WaterDunkMeta);
		
		AnvilDropMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Anvil Drop Sound");
		AnvilDropMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		AnvilDrop.setItemMeta(AnvilDropMeta);
		
		PistonExtendingMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Piston Extending Sound");
		PistonExtendingMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		PistonExtending.setItemMeta(PistonExtendingMeta);
		
		CatMeowMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Cat Meow Sound");
		CatMeowMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2500 Electros."));
		CatMeow.setItemMeta(CatMeowMeta);
		
		ExplodingTNTMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Exploding TNT Sound");
		ExplodingTNTMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		ExplodingTNT.setItemMeta(ExplodingTNTMeta);
		
		ChestOpeningMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Chest Opening Sound");
		ChestOpeningMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		ChestOpening.setItemMeta(ChestOpeningMeta);
		
		GhastDeathMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Ghast Death Sound");
		GhastDeathMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 3000 Electros."));
		GhastDeath.setItemMeta(GhastDeathMeta);
		
		LightningMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Lightning Sound");
		LightningMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Donator Exclusive!",ChatColor.GOLD + "▶ Left-click to equip."));
		Lightning.setItemMeta(LightningMeta);
		
		DogBarkMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Dog Bark Sound");
		DogBarkMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2500 Electros."));
		DogBark.setItemMeta(DogBarkMeta);
		
		ResetJoinSoundMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Reset Join Sound");
		ResetJoinSoundMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to reset your join sound."));
		ResetJoinSound.setItemMeta(ResetJoinSoundMeta);
		
		MainMenuMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Main Menu");
		MainMenuMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to return to the main menu."));
		MainMenu.setItemMeta(MainMenuMeta);
		
		// Setting The Items
		PlayerWardrobeJoinSoundsGUI.setItem(0, WaterDunk);
		PlayerWardrobeJoinSoundsGUI.setItem(1, AnvilDrop);
		PlayerWardrobeJoinSoundsGUI.setItem(2, PistonExtending);
		PlayerWardrobeJoinSoundsGUI.setItem(3, CatMeow);
		PlayerWardrobeJoinSoundsGUI.setItem(4, ExplodingTNT);
		PlayerWardrobeJoinSoundsGUI.setItem(5, ChestOpening);
		PlayerWardrobeJoinSoundsGUI.setItem(6, GhastDeath);
		PlayerWardrobeJoinSoundsGUI.setItem(7, Lightning);
		PlayerWardrobeJoinSoundsGUI.setItem(8, DogBark);
		PlayerWardrobeJoinSoundsGUI.setItem(11, ResetJoinSound);
		PlayerWardrobeJoinSoundsGUI.setItem(13, MainMenu);
		PlayerWardrobeJoinSoundsGUI.setItem(15, ResetJoinSound);
		
		player.openInventory(PlayerWardrobeJoinSoundsGUI);
	}
	public void PlayerWardrobeParticlesGUI(Player player) {
		Inventory PlayerWardrobeParticlesGUI = Bukkit.createInventory(null, 18, ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Particles");
		// Items
		ItemStack FlameParticle = new ItemStack(Material.BLAZE_POWDER);
		ItemMeta FlameParticleMeta = FlameParticle.getItemMeta();
		
		ItemStack SlimeParticle = new ItemStack(Material.SLIME_BALL);
		ItemMeta SlimeParticleMeta = SlimeParticle.getItemMeta();
		
		ItemStack CriticalParticle = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta CriticalParticleMeta = CriticalParticle.getItemMeta();
		
		ItemStack EnderSignalParticle = new ItemStack(Material.ENDER_PEARL);
		ItemMeta EnderSignalParticleMeta = EnderSignalParticle.getItemMeta();
		
		ItemStack FootstepParticle = new ItemStack(Material.IRON_BOOTS);
		ItemMeta FootstepParticleMeta = FootstepParticle.getItemMeta();
		
		ItemStack HeartParticle = new ItemStack(Material.WHEAT);
		ItemMeta HeartParticleMeta = HeartParticle.getItemMeta();
		
		ItemStack LavaDripParticle = new ItemStack(Material.LAVA_BUCKET);
		ItemMeta LavaDripParticleMeta = LavaDripParticle.getItemMeta();
		
		ItemStack WaterDripParticle = new ItemStack(Material.WATER_BUCKET);
		ItemMeta WaterDripParticleMeta = WaterDripParticle.getItemMeta();
		
		ItemStack NetherPortalParticle = new ItemStack(Material.FLINT_AND_STEEL);
		ItemMeta NetherPortalParticleMeta = NetherPortalParticle.getItemMeta();
		
		ItemStack ResetParticle = new ItemStack(Material.TNT);
		ItemMeta ResetParticleMeta = ResetParticle.getItemMeta();
		
		ItemStack MainMenu = new ItemStack(Material.PAINTING);
		ItemMeta MainMenuMeta = MainMenu.getItemMeta();
		
		// Display Names
		FlameParticleMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Flame Particle");
		FlameParticleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 3000 Electros."));
		FlameParticle.setItemMeta(FlameParticleMeta);
		
		SlimeParticleMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Slime Particle");
		SlimeParticleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1000 Electros."));
		SlimeParticle.setItemMeta(SlimeParticleMeta);
		
		CriticalParticleMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Critical Particle");
		CriticalParticleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1500 Electros."));
		CriticalParticle.setItemMeta(CriticalParticleMeta);
		
		EnderSignalParticleMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Ender Signal Particle");
		EnderSignalParticleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 2000 Electros."));
		EnderSignalParticle.setItemMeta(EnderSignalParticleMeta);
		
		FootstepParticleMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Footstep Particle");
		FootstepParticleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 500 Electros."));
		FootstepParticle.setItemMeta(FootstepParticleMeta);
		
		HeartParticleMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Heart Particle");
		HeartParticleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1500 Electros."));
		HeartParticle.setItemMeta(HeartParticleMeta);
		
		LavaDripParticleMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Lava Drip Particle");
		LavaDripParticleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1250 Electros."));
		LavaDripParticle.setItemMeta(LavaDripParticleMeta);
		
		WaterDripParticleMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Water Drip Particle");
		WaterDripParticleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1250 Electros."));
		WaterDripParticle.setItemMeta(WaterDripParticleMeta);
		
		NetherPortalParticleMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Nether Portal Particle");
		NetherPortalParticleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to equip.",ChatColor.GOLD + "▶ Right-click to buy.",ChatColor.GOLD + "▶ Costs 1500 Electros."));
		NetherPortalParticle.setItemMeta(NetherPortalParticleMeta);
		
		ResetParticleMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Reset Particle");
		ResetParticleMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to reset your particle effect."));
		ResetParticle.setItemMeta(ResetParticleMeta);
		
		MainMenuMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Main Menu");
		MainMenuMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Left-click to return to the main menu."));
		MainMenu.setItemMeta(MainMenuMeta);
		
		// Setting The Items
		PlayerWardrobeParticlesGUI.setItem(0, FlameParticle);
		PlayerWardrobeParticlesGUI.setItem(1, SlimeParticle);
		PlayerWardrobeParticlesGUI.setItem(2, CriticalParticle);
		PlayerWardrobeParticlesGUI.setItem(3, EnderSignalParticle);
		PlayerWardrobeParticlesGUI.setItem(4, FootstepParticle);
		PlayerWardrobeParticlesGUI.setItem(5, HeartParticle);
		PlayerWardrobeParticlesGUI.setItem(6, LavaDripParticle);
		PlayerWardrobeParticlesGUI.setItem(7, WaterDripParticle);
		PlayerWardrobeParticlesGUI.setItem(8, NetherPortalParticle);
		PlayerWardrobeParticlesGUI.setItem(11, ResetParticle);
		PlayerWardrobeParticlesGUI.setItem(13, MainMenu);
		PlayerWardrobeParticlesGUI.setItem(15, ResetParticle);
		
		player.openInventory(PlayerWardrobeParticlesGUI);
	}
	@EventHandler
	public void PlayerWardrobeInteract(PlayerInteractEvent e) {
		if((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
			ItemStack currentItem = e.getPlayer().getItemInHand();
			if((currentItem == null) || (currentItem.getType() == Material.AIR)) {
				return;
			}
			if((!currentItem.hasItemMeta()) || ((currentItem.hasItemMeta()) && (!currentItem.getItemMeta().hasDisplayName()))) {
				return;
			}
			if(currentItem.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe")) {
				Player player = (Player) e.getPlayer();
				PlayerWardrobeGUI(player);
			}
		}
	}
	@EventHandler
	public void PlayerWardrobeInventoryClick(InventoryClickEvent e) {
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		Player player = (Player) e.getWhoClicked();
		if(!e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe")) return;
		if(e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe")) {
			e.setCancelled(true);
			if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Clothes")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					PlayerWardrobeClothesGUI(player);
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You need to be a donator to access the clothes selection menu.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Hats")) {
				PlayerWardrobeHatsGUI(player);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Armor")) {
				PlayerWardrobeArmorGUI(player);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Join Sounds")) {
				PlayerWardrobeJoinSoundsGUI(player);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Particles")) {
				PlayerWardrobeParticlesGUI(player);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Gadgets")) {
				player.sendMessage(prefix + ChatColor.RED + "Sorry, this section in the player wardrobe is coming soon.");
				Location loc = player.getLocation();
				player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Morphs")) {
				player.sendMessage(prefix + ChatColor.RED + "Sorry, this section in the player wardrobe is coming soon.");
				Location loc = player.getLocation();
				player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Banner Hats")) {
				player.sendMessage(prefix + ChatColor.RED + "Sorry, this section in the player wardrobe is coming soon.");
				Location loc = player.getLocation();
				player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
			}
		}
	}
	@EventHandler
	public void PlayerWardrobeParticlesInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		List<String> Purchases = this.plugin.electrodatabase.getStringList("Players." + player.getUniqueId() + ".Purchased");
		if(!e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Particles")) return;
		if(e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Particles")) {
			e.setCancelled(true);
			if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Main Menu")) {
				PlayerWardrobeGUI(player);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Reset Particle")) {
				Location loc = player.getLocation();
				player.playSound(loc, Sound.EXPLODE, 1, 3);
				this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Particle", "None");
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Flame Particle")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("FlameParticle")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Particle", "Flame");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") >= 3000) {
						if(!(Purchases.contains("FlameParticle"))) {
							Purchases.add("FlameParticle");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Flame Particle");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 3000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Slime Particle")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("SlimeParticle")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Particle", "Slime");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1000) {
						if(!(Purchases.contains("SlimeParticle"))) {
							Purchases.add("SlimeParticle");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Slime Particle");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Critical Particle")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("CriticalParticle")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Particle", "Critical");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1500) {
						if(!Purchases.contains("CriticalParticle")) {
							Purchases.add("CriticalParticle");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Critical Particle");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Ender Signal Particle")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("EnderSignalParticle")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Particle", "EnderSignal");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("EnderSignalParticle")) {
							Purchases.add("EnderSignalParticle");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Ender Signal Particle");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Footstep Particle")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("FootstepParticle")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Particle", "Footstep");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 500) {
						if(!Purchases.contains("FootstepParticle")) {
							Purchases.add("FootstepParticle");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Footstep Particle");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Heart Particle")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("HeartParticle")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Particle", "Heart");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1500) {
						if(!Purchases.contains("HeartParticle")) {
							Purchases.add("HeartParticle");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Heart Particle");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Lava Drip Particle")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("LavaDripParticle")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Particle", "LavaDrip");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1250) {
						if(!Purchases.contains("LavaDripParticle")) {
							Purchases.add("LavaDripParticle");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Lava Drip Particle");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1250));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Water Drip Particle")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("WaterDripParticle")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Particle", "WaterDrip");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1250) {
						if(!Purchases.contains("WaterDripParticle")) {
							Purchases.add("WaterDripParticle");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Water Drip Particle");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1250));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Nether Portal Particle")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("NetherPortalParticle")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Particle", "NetherPortal");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1500) {
						if(!Purchases.contains("NetherPortalParticle")) {
							Purchases.add("NetherPortalParticle");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Nether Portal Particle");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}
		}
	}
	@EventHandler
	public void PlayerWardrobeClothesInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		if(!e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Clothes")) return;
		if(e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Clothes")) {
			e.setCancelled(true);
			if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Main Menu")) {
				PlayerWardrobeGUI(player);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Reset Clothes")) {
				ItemStack ResetClothes = new ItemStack(Material.AIR);
				ItemMeta ResetClothesMeta = ResetClothes.getItemMeta();
				ResetClothes.setItemMeta(ResetClothesMeta);
				player.getInventory().setHelmet(ResetClothes);
				player.getInventory().setChestplate(ResetClothes);
				player.getInventory().setLeggings(ResetClothes);
				player.getInventory().setBoots(ResetClothes);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.EXPLODE, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Red Hat")) {
				ItemStack REDHat = new ItemStack(Material.LEATHER_HELMET);
				LeatherArmorMeta REDHatMeta = (LeatherArmorMeta) REDHat.getItemMeta();
				REDHatMeta.setColor(Color.RED);
				REDHat.setItemMeta(REDHatMeta);
				player.getInventory().setHelmet(REDHat);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Red Shirt")) {
				ItemStack REDShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta REDShirtMeta = (LeatherArmorMeta) REDShirt.getItemMeta();
				REDShirtMeta.setColor(Color.RED);
				REDShirt.setItemMeta(REDShirtMeta);
				player.getInventory().setChestplate(REDShirt);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Red Trousers")) {
				ItemStack REDTrousers = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta REDTrousersMeta = (LeatherArmorMeta) REDTrousers.getItemMeta();
				REDTrousersMeta.setColor(Color.RED);
				REDTrousers.setItemMeta(REDTrousersMeta);
				player.getInventory().setLeggings(REDTrousers);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Red Shoes")) {
				ItemStack REDShoes = new ItemStack(Material.LEATHER_BOOTS);
				LeatherArmorMeta REDShoesMeta = (LeatherArmorMeta) REDShoes.getItemMeta();
				REDShoesMeta.setColor(Color.RED);
				REDShoes.setItemMeta(REDShoesMeta);
				player.getInventory().setBoots(REDShoes);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Blue Hat")) {
				ItemStack BLUEHat = new ItemStack(Material.LEATHER_HELMET);
				LeatherArmorMeta BLUEHatMeta = (LeatherArmorMeta) BLUEHat.getItemMeta();
				BLUEHatMeta.setColor(Color.BLUE);
				BLUEHat.setItemMeta(BLUEHatMeta);
				player.getInventory().setHelmet(BLUEHat);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Blue Shirt")) {
				ItemStack BLUEShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta BLUEShirtMeta = (LeatherArmorMeta) BLUEShirt.getItemMeta();
				BLUEShirtMeta.setColor(Color.BLUE);
				BLUEShirt.setItemMeta(BLUEShirtMeta);
				player.getInventory().setChestplate(BLUEShirt);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Blue Trousers")) {
				ItemStack BLUETrousers = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta BLUETrousersMeta = (LeatherArmorMeta) BLUETrousers.getItemMeta();
				BLUETrousersMeta.setColor(Color.BLUE);
				BLUETrousers.setItemMeta(BLUETrousersMeta);
				player.getInventory().setLeggings(BLUETrousers);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Blue Shoes")) {
				ItemStack BLUEShoes = new ItemStack(Material.LEATHER_BOOTS);
				LeatherArmorMeta BLUEShoesMeta = (LeatherArmorMeta) BLUEShoes.getItemMeta();
				BLUEShoesMeta.setColor(Color.BLUE);
				BLUEShoes.setItemMeta(BLUEShoesMeta);
				player.getInventory().setBoots(BLUEShoes);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Green Hat")) {
				ItemStack GREENHat = new ItemStack(Material.LEATHER_HELMET);
				LeatherArmorMeta GREENHatMeta = (LeatherArmorMeta) GREENHat.getItemMeta();
				GREENHatMeta.setColor(Color.GREEN);
				GREENHat.setItemMeta(GREENHatMeta);
				player.getInventory().setHelmet(GREENHat);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Green Shirt")) {
				ItemStack GREENShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta GREENShirtMeta = (LeatherArmorMeta) GREENShirt.getItemMeta();
				GREENShirtMeta.setColor(Color.GREEN);
				GREENShirt.setItemMeta(GREENShirtMeta);
				player.getInventory().setChestplate(GREENShirt);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Green Trousers")) {
				ItemStack GREENTrousers = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta GREENTrousersMeta = (LeatherArmorMeta) GREENTrousers.getItemMeta();
				GREENTrousersMeta.setColor(Color.GREEN);
				GREENTrousers.setItemMeta(GREENTrousersMeta);
				player.getInventory().setLeggings(GREENTrousers);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Green Shoes")) {
				ItemStack GREENShoes = new ItemStack(Material.LEATHER_BOOTS);
				LeatherArmorMeta GREENShoesMeta = (LeatherArmorMeta) GREENShoes.getItemMeta();
				GREENShoesMeta.setColor(Color.GREEN);
				GREENShoes.setItemMeta(GREENShoesMeta);
				player.getInventory().setBoots(GREENShoes);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Yellow Hat")) {
				ItemStack YELLOWHat = new ItemStack(Material.LEATHER_HELMET);
				LeatherArmorMeta YELLOWHatMeta = (LeatherArmorMeta) YELLOWHat.getItemMeta();
				YELLOWHatMeta.setColor(Color.YELLOW);
				YELLOWHat.setItemMeta(YELLOWHatMeta);
				player.getInventory().setHelmet(YELLOWHat);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Yellow Shirt")) {
				ItemStack YELLOWShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta YELLOWShirtMeta = (LeatherArmorMeta) YELLOWShirt.getItemMeta();
				YELLOWShirtMeta.setColor(Color.YELLOW);
				YELLOWShirt.setItemMeta(YELLOWShirtMeta);
				player.getInventory().setChestplate(YELLOWShirt);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Yellow Trousers")) {
				ItemStack YELLOWTrousers = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta YELLOWTrousersMeta = (LeatherArmorMeta) YELLOWTrousers.getItemMeta();
				YELLOWTrousersMeta.setColor(Color.YELLOW);
				YELLOWTrousers.setItemMeta(YELLOWTrousersMeta);
				player.getInventory().setLeggings(YELLOWTrousers);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Yellow Shoes")) {
				ItemStack YELLOWShoes = new ItemStack(Material.LEATHER_BOOTS);
				LeatherArmorMeta YELLOWShoesMeta = (LeatherArmorMeta) YELLOWShoes.getItemMeta();
				YELLOWShoesMeta.setColor(Color.YELLOW);
				YELLOWShoes.setItemMeta(YELLOWShoesMeta);
				player.getInventory().setBoots(YELLOWShoes);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Purple Hat")) {
				ItemStack PURPLEHat = new ItemStack(Material.LEATHER_HELMET);
				LeatherArmorMeta PURPLEHatMeta = (LeatherArmorMeta) PURPLEHat.getItemMeta();
				PURPLEHatMeta.setColor(Color.PURPLE);
				PURPLEHat.setItemMeta(PURPLEHatMeta);
				player.getInventory().setHelmet(PURPLEHat);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Purple Shirt")) {
				ItemStack PURPLEShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta PURPLEShirtMeta = (LeatherArmorMeta) PURPLEShirt.getItemMeta();
				PURPLEShirtMeta.setColor(Color.PURPLE);
				PURPLEShirt.setItemMeta(PURPLEShirtMeta);
				player.getInventory().setChestplate(PURPLEShirt);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Purple Trousers")) {
				ItemStack PURPLETrousers = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta PURPLETrousersMeta = (LeatherArmorMeta) PURPLETrousers.getItemMeta();
				PURPLETrousersMeta.setColor(Color.PURPLE);
				PURPLETrousers.setItemMeta(PURPLETrousersMeta);
				player.getInventory().setLeggings(PURPLETrousers);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Purple Shoes")) {
				ItemStack PURPLEShoes = new ItemStack(Material.LEATHER_BOOTS);
				LeatherArmorMeta PURPLEShoesMeta = (LeatherArmorMeta) PURPLEShoes.getItemMeta();
				PURPLEShoesMeta.setColor(Color.PURPLE);
				PURPLEShoes.setItemMeta(PURPLEShoesMeta);
				player.getInventory().setBoots(PURPLEShoes);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Orange Hat")) {
				ItemStack ORANGEHat = new ItemStack(Material.LEATHER_HELMET);
				LeatherArmorMeta ORANGEHatMeta = (LeatherArmorMeta) ORANGEHat.getItemMeta();
				ORANGEHatMeta.setColor(Color.ORANGE);
				ORANGEHat.setItemMeta(ORANGEHatMeta);
				player.getInventory().setHelmet(ORANGEHat);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Orange Shirt")) {
				ItemStack ORANGEShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta ORANGEShirtMeta = (LeatherArmorMeta) ORANGEShirt.getItemMeta();
				ORANGEShirtMeta.setColor(Color.ORANGE);
				ORANGEShirt.setItemMeta(ORANGEShirtMeta);
				player.getInventory().setChestplate(ORANGEShirt);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Orange Trousers")) {
				ItemStack ORANGETrousers = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta ORANGETrousersMeta = (LeatherArmorMeta) ORANGETrousers.getItemMeta();
				ORANGETrousersMeta.setColor(Color.ORANGE);
				ORANGETrousers.setItemMeta(ORANGETrousersMeta);
				player.getInventory().setLeggings(ORANGETrousers);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Orange Shoes")) {
				ItemStack ORANGEShoes = new ItemStack(Material.LEATHER_BOOTS);
				LeatherArmorMeta ORANGEShoesMeta = (LeatherArmorMeta) ORANGEShoes.getItemMeta();
				ORANGEShoesMeta.setColor(Color.ORANGE);
				ORANGEShoes.setItemMeta(ORANGEShoesMeta);
				player.getInventory().setBoots(ORANGEShoes);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.WHITE + "" + ChatColor.BOLD + "White Hat")) {
				ItemStack WHITEHat = new ItemStack(Material.LEATHER_HELMET);
				LeatherArmorMeta WHITEHatMeta = (LeatherArmorMeta) WHITEHat.getItemMeta();
				WHITEHatMeta.setColor(Color.WHITE);
				WHITEHat.setItemMeta(WHITEHatMeta);
				player.getInventory().setHelmet(WHITEHat);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.WHITE + "" + ChatColor.BOLD + "White Shirt")) {
				ItemStack WHITEShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta WHITEShirtMeta = (LeatherArmorMeta) WHITEShirt.getItemMeta();
				WHITEShirtMeta.setColor(Color.WHITE);
				WHITEShirt.setItemMeta(WHITEShirtMeta);
				player.getInventory().setChestplate(WHITEShirt);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.WHITE + "" + ChatColor.BOLD + "White Trousers")) {
				ItemStack WHITETrousers = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta WHITETrousersMeta = (LeatherArmorMeta) WHITETrousers.getItemMeta();
				WHITETrousersMeta.setColor(Color.WHITE);
				WHITETrousers.setItemMeta(WHITETrousersMeta);
				player.getInventory().setLeggings(WHITETrousers);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.WHITE + "" + ChatColor.BOLD + "White Shoes")) {
				ItemStack WHITEShoes = new ItemStack(Material.LEATHER_BOOTS);
				LeatherArmorMeta WHITEShoesMeta = (LeatherArmorMeta) WHITEShoes.getItemMeta();
				WHITEShoesMeta.setColor(Color.WHITE);
				WHITEShoes.setItemMeta(WHITEShoesMeta);
				player.getInventory().setBoots(WHITEShoes);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.BOLD + "Lime Hat")) {
				ItemStack LIMEHat = new ItemStack(Material.LEATHER_HELMET);
				LeatherArmorMeta LIMEHatMeta = (LeatherArmorMeta) LIMEHat.getItemMeta();
				LIMEHatMeta.setColor(Color.LIME);
				LIMEHat.setItemMeta(LIMEHatMeta);
				player.getInventory().setHelmet(LIMEHat);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.BOLD + "Lime Shirt")) {
				ItemStack LIMEShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta LIMEShirtMeta = (LeatherArmorMeta) LIMEShirt.getItemMeta();
				LIMEShirtMeta.setColor(Color.LIME);
				LIMEShirt.setItemMeta(LIMEShirtMeta);
				player.getInventory().setChestplate(LIMEShirt);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.BOLD + "Lime Trousers")) {
				ItemStack LIMETrousers = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta LIMETrousersMeta = (LeatherArmorMeta) LIMETrousers.getItemMeta();
				LIMETrousersMeta.setColor(Color.LIME);
				LIMETrousers.setItemMeta(LIMETrousersMeta);
				player.getInventory().setLeggings(LIMETrousers);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.BOLD + "Lime Shoes")) {
				ItemStack LIMEShoes = new ItemStack(Material.LEATHER_BOOTS);
				LeatherArmorMeta LIMEShoesMeta = (LeatherArmorMeta) LIMEShoes.getItemMeta();
				LIMEShoesMeta.setColor(Color.LIME);
				LIMEShoes.setItemMeta(LIMEShoesMeta);
				player.getInventory().setBoots(LIMEShoes);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Pink Hat")) {
				ItemStack FUCHSIAHat = new ItemStack(Material.LEATHER_HELMET);
				LeatherArmorMeta FUCHSIAHatMeta = (LeatherArmorMeta) FUCHSIAHat.getItemMeta();
				FUCHSIAHatMeta.setColor(Color.FUCHSIA);
				FUCHSIAHat.setItemMeta(FUCHSIAHatMeta);
				player.getInventory().setHelmet(FUCHSIAHat);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Pink Shirt")) {
				ItemStack FUCHSIAShirt = new ItemStack(Material.LEATHER_CHESTPLATE);
				LeatherArmorMeta FUCHSIAShirtMeta = (LeatherArmorMeta) FUCHSIAShirt.getItemMeta();
				FUCHSIAShirtMeta.setColor(Color.FUCHSIA);
				FUCHSIAShirt.setItemMeta(FUCHSIAShirtMeta);
				player.getInventory().setChestplate(FUCHSIAShirt);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Pink Trousers")) {
				ItemStack FUCHSIATrousers = new ItemStack(Material.LEATHER_LEGGINGS);
				LeatherArmorMeta FUCHSIATrousersMeta = (LeatherArmorMeta) FUCHSIATrousers.getItemMeta();
				FUCHSIATrousersMeta.setColor(Color.FUCHSIA);
				FUCHSIATrousers.setItemMeta(FUCHSIATrousersMeta);
				player.getInventory().setLeggings(FUCHSIATrousers);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Pink Shoes")) {
				ItemStack FUCHSIAShoes = new ItemStack(Material.LEATHER_BOOTS);
				LeatherArmorMeta FUCHSIAShoesMeta = (LeatherArmorMeta) FUCHSIAShoes.getItemMeta();
				FUCHSIAShoesMeta.setColor(Color.FUCHSIA);
				FUCHSIAShoes.setItemMeta(FUCHSIAShoesMeta);
				player.getInventory().setBoots(FUCHSIAShoes);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.NOTE_PLING, 1, 3);
			}
		}
	}
	@EventHandler
	public void PlayerWardrobeHatsInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		List<String> Purchases = this.plugin.electrodatabase.getStringList("Players." + player.getUniqueId() + ".Purchased");
		if(!e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Hats")) return;
		if(e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Hats")) {
			e.setCancelled(true);
		if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Reset Hat")) {
			ItemStack ResetHat = new ItemStack(Material.AIR);
			ItemMeta ResetHatMeta = ResetHat.getItemMeta();
			ResetHat.setItemMeta(ResetHatMeta);
			player.getInventory().setHelmet(ResetHat);
			Location loc = player.getLocation();
			player.playSound(loc, Sound.EXPLODE, 1, 3);
		}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Main Menu")) {
			PlayerWardrobeGUI(player);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Crafting Table Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("CraftingTableHat")) {
						ItemStack CraftingTable = new ItemStack(Material.WORKBENCH);
						ItemMeta CraftingTableMeta = CraftingTable.getItemMeta();
						CraftingTable.setItemMeta(CraftingTableMeta);
						player.getInventory().setHelmet(CraftingTable);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1000) {
						if(!Purchases.contains("CraftingTableHat")) {
							Purchases.add("CraftingTableHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Crafting Table Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "TNT Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("TNTHat")) {
						ItemStack TNT = new ItemStack(Material.TNT);
						ItemMeta TNTMeta = TNT.getItemMeta();
						TNT.setItemMeta(TNTMeta);
						player.getInventory().setHelmet(TNT);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("TNTHat")) {
							Purchases.add("TNTHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "TNT Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Hay Bale Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("HayBaleHat")) {
						ItemStack HayBale = new ItemStack(Material.HAY_BLOCK);
						ItemMeta HayBaleMeta = HayBale.getItemMeta();
						HayBale.setItemMeta(HayBaleMeta);
						player.getInventory().setHelmet(HayBale);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1000) {
						if(!Purchases.contains("HayBaleHat")) {
							Purchases.add("HayBaleHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Hay Bale Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Melon Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("MelonHat")) {
						ItemStack Melon = new ItemStack(Material.MELON_BLOCK);
						ItemMeta MelonMeta = Melon.getItemMeta();
						Melon.setItemMeta(MelonMeta);
						player.getInventory().setHelmet(Melon);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1500) {
						if(!Purchases.contains("MelonHat")) {
							Purchases.add("MelonHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Melon Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Redstone Lamp Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("LampHat")) {
						ItemStack RedstoneLamp = new ItemStack(Material.REDSTONE_LAMP_OFF);
						ItemMeta RedstoneLampMeta = RedstoneLamp.getItemMeta();
						RedstoneLamp.setItemMeta(RedstoneLampMeta);
						player.getInventory().setHelmet(RedstoneLamp);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("LampHat")) {
							Purchases.add("LampHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Lamp Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Sea Lantern Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("SeaLanternHat")) {
						ItemStack SeaLantern = new ItemStack(Material.SEA_LANTERN);
						ItemMeta SeaLanternMeta = SeaLantern.getItemMeta();
						SeaLantern.setItemMeta(SeaLanternMeta);
						player.getInventory().setHelmet(SeaLantern);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2500) {
						if(!Purchases.contains("SeaLanternHat")) {
							Purchases.add("SeaLanternHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Sea Lantern Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Jack O'Lantern Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("PumpkinGlowingHat")) {
						ItemStack PumpkinGlowing = new ItemStack(Material.JACK_O_LANTERN);
						ItemMeta PumpkinGlowingMeta = PumpkinGlowing.getItemMeta();
						PumpkinGlowing.setItemMeta(PumpkinGlowingMeta);
						player.getInventory().setHelmet(PumpkinGlowing);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2500) {
						if(!Purchases.contains("PumpkinGlowingHat")) {
							Purchases.add("PumpkinGlowingHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Jack O'Lantern Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Pumpkin Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("PumpkinHat")) {
						ItemStack Pumpkin = new ItemStack(Material.PUMPKIN);
						ItemMeta PumpkinMeta = Pumpkin.getItemMeta();
						Pumpkin.setItemMeta(PumpkinMeta);
						player.getInventory().setHelmet(Pumpkin);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1500) {
						if(!Purchases.contains("PumpkinHat")) {
							Purchases.add("PumpkinHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Pumpkin Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Slime Block Hat")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack SlimeBlock = new ItemStack(Material.SLIME_BLOCK);
						ItemMeta SlimeBlockMeta = SlimeBlock.getItemMeta();
						SlimeBlock.setItemMeta(SlimeBlockMeta);
						player.getInventory().setHelmet(SlimeBlock);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Furnace Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("FurnaceHat")) {
						ItemStack Furnace = new ItemStack(Material.FURNACE);
						ItemMeta FurnaceMeta = Furnace.getItemMeta();
						Furnace.setItemMeta(FurnaceMeta);
						player.getInventory().setHelmet(Furnace);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("FurnaceHat")) {
							Purchases.add("FurnaceHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Furnace Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Dispenser Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("DispenserHat")) {
						ItemStack Dispenser = new ItemStack(Material.DISPENSER);
						ItemMeta DispenserMeta = Dispenser.getItemMeta();
						Dispenser.setItemMeta(DispenserMeta);
						player.getInventory().setHelmet(Dispenser);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("DispenserHat")) {
							Purchases.add("DispenserHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Dispenser Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Dropper Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("DropperHat")) {
						ItemStack Dropper = new ItemStack(Material.DROPPER);
						ItemMeta DropperMeta = Dropper.getItemMeta();
						Dropper.setItemMeta(DropperMeta);
						player.getInventory().setHelmet(Dropper);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("DropperHat")) {
							Purchases.add("DropperHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Dropper Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Note Block Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("NoteBlockHat")) {
						ItemStack NoteBlock = new ItemStack(Material.NOTE_BLOCK);
						ItemMeta NoteBlockMeta = NoteBlock.getItemMeta();
						NoteBlock.setItemMeta(NoteBlockMeta);
						player.getInventory().setHelmet(NoteBlock);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 3000) {
						if(!Purchases.contains("NoteBlockHat")) {
							Purchases.add("NoteBlockHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Note Block Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 3000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Piston Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("PistonHat")) {
						ItemStack Piston = new ItemStack(Material.PISTON_BASE);
						ItemMeta PistonMeta = Piston.getItemMeta();
						Piston.setItemMeta(PistonMeta);
						player.getInventory().setHelmet(Piston);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 3000) {
						if(!Purchases.contains("PistonHat")) {
							Purchases.add("PistonHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Piston Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 3000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Sponge Hat")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack Sponge = new ItemStack(Material.SPONGE);
						ItemMeta SpongeMeta = Sponge.getItemMeta();
						Sponge.setItemMeta(SpongeMeta);
						player.getInventory().setHelmet(Sponge);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Diamond Block Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("DiamondBlockHat")) {
						ItemStack DiamondBlock = new ItemStack(Material.DIAMOND_BLOCK);
						ItemMeta DiamondBlockMeta = DiamondBlock.getItemMeta();
						DiamondBlock.setItemMeta(DiamondBlockMeta);
						player.getInventory().setHelmet(DiamondBlock);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 3500) {
						if(!Purchases.contains("DiamondBlockHat")) {
							Purchases.add("DiamondBlockHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Diamond Block Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 3500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Emerald Block Hat")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack EmeraldBlock = new ItemStack(Material.EMERALD_BLOCK);
						ItemMeta EmeraldBlockMeta = EmeraldBlock.getItemMeta();
						EmeraldBlock.setItemMeta(EmeraldBlockMeta);
						player.getInventory().setHelmet(EmeraldBlock);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold Block Hat")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("GoldBlockHat")) {
						ItemStack GoldBlock = new ItemStack(Material.GOLD_BLOCK);
						ItemMeta GoldBlockMeta = GoldBlock.getItemMeta();
						GoldBlock.setItemMeta(GoldBlockMeta);
						player.getInventory().setHelmet(GoldBlock);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 3000) {
						if(!Purchases.contains("GoldBlockHat")) {
							Purchases.add("GoldBlockHat");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Gold Block Hat");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 3000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}
		}
	}
	@EventHandler
	public void PlayerWardrobeArmorInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		List<String> Purchases = this.plugin.electrodatabase.getStringList("Players." + player.getUniqueId() + ".Purchased");
		if(!e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Armor")) return;
		if(e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - Armor")) {
			e.setCancelled(true);
			if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Main Menu")) {
				PlayerWardrobeGUI(player);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Reset Armor")) {
				ItemStack ResetArmor = new ItemStack(Material.AIR);
				ItemMeta ResetArmorMeta = ResetArmor.getItemMeta();
				ResetArmor.setItemMeta(ResetArmorMeta);
				player.getInventory().setHelmet(ResetArmor);
				player.getInventory().setChestplate(ResetArmor);
				player.getInventory().setLeggings(ResetArmor);
				player.getInventory().setBoots(ResetArmor);
				Location loc = player.getLocation();
				player.playSound(loc, Sound.EXPLODE, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Leather Helmet")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("LeatherHelmet")) {
						ItemStack LeatherHelmet = new ItemStack(Material.LEATHER_HELMET);
						ItemMeta LeatherHelmetMeta = LeatherHelmet.getItemMeta();
						LeatherHelmet.setItemMeta(LeatherHelmetMeta);
						player.getInventory().setHelmet(LeatherHelmet);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 500) {
						if(!Purchases.contains("LeatherHelmet")) {
							Purchases.add("LeatherHelmet");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Leather Helmet");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Leather Chestplate")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("LeatherChestplate")) {
						ItemStack LeatherChestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
						ItemMeta LeatherChestplateMeta = LeatherChestplate.getItemMeta();
						LeatherChestplate.setItemMeta(LeatherChestplateMeta);
						player.getInventory().setChestplate(LeatherChestplate);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1000) {
						if(!Purchases.contains("LeatherChestplate")) {
							Purchases.add("LeatherChestplate");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Leather Chestplate");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Leather Leggings")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("LeatherLeggings")) {
						ItemStack LeatherLeggings = new ItemStack(Material.LEATHER_LEGGINGS);
						ItemMeta LeatherLeggingsMeta = LeatherLeggings.getItemMeta();
						LeatherLeggings.setItemMeta(LeatherLeggingsMeta);
						player.getInventory().setLeggings(LeatherLeggings);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("LeatherLeggings")) {
							Purchases.add("LeatherLeggings");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Leather Leggings");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Leather Boots")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("LeatherBoots")) {
						ItemStack LeatherBoots = new ItemStack(Material.LEATHER_BOOTS);
						ItemMeta LeatherBootsMeta = LeatherBoots.getItemMeta();
						LeatherBoots.setItemMeta(LeatherBootsMeta);
						player.getInventory().setBoots(LeatherBoots);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 500) {
						if(!Purchases.contains("LeatherBoots")) {
							Purchases.add("LeatherBoots");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Leather Boots");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Chain Helmet")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("ChainHelmet")) {
						ItemStack ChainHelmet = new ItemStack(Material.CHAINMAIL_HELMET);
						ItemMeta ChainHelmetMeta = ChainHelmet.getItemMeta();
						ChainHelmet.setItemMeta(ChainHelmetMeta);
						player.getInventory().setHelmet(ChainHelmet);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 600) {
						if(!Purchases.contains("ChainHelmet")) {
							Purchases.add("ChainHelmet");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Chain Helmet");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 600));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Chain Chestplate")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("ChainChestplate")) {
						ItemStack ChainChestplate = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
						ItemMeta ChainChestplateMeta = ChainChestplate.getItemMeta();
						ChainChestplate.setItemMeta(ChainChestplateMeta);
						player.getInventory().setChestplate(ChainChestplate);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1100) {
						if(!Purchases.contains("ChainChestplate")) {
							Purchases.add("ChainChestplate");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Chain Chestplate");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1100));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Chain Leggings")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("ChainLeggings")) {
						ItemStack ChainLeggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
						ItemMeta ChainLeggingsMeta = ChainLeggings.getItemMeta();
						ChainLeggings.setItemMeta(ChainLeggingsMeta);
						player.getInventory().setLeggings(ChainLeggings);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 850) {
						if(!Purchases.contains("ChainLeggings")) {
							Purchases.add("ChainLeggings");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Chain Leggings");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 850));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Chain Boots")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("ChainBoots")) {
						ItemStack ChainBoots = new ItemStack(Material.CHAINMAIL_BOOTS);
						ItemMeta ChainBootsMeta = ChainBoots.getItemMeta();
						ChainBoots.setItemMeta(ChainBootsMeta);
						player.getInventory().setBoots(ChainBoots);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 600) {
						if(!Purchases.contains("ChainBoots")) {
							Purchases.add("ChainBoots");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Chain Boots");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 600));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Iron Helmet")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("IronHelmet")) {
						ItemStack IronHelmet = new ItemStack(Material.IRON_HELMET);
						ItemMeta IronHelmetMeta = IronHelmet.getItemMeta();
						IronHelmet.setItemMeta(IronHelmetMeta);
						player.getInventory().setHelmet(IronHelmet);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("IronHelmet")) {
							Purchases.add("IronHelmet");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Iron Helmet");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Iron Chestplate")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("IronChestplate")) {
						ItemStack IronChestplate = new ItemStack(Material.IRON_CHESTPLATE);
						ItemMeta IronChestplateMeta = IronChestplate.getItemMeta();
						IronChestplate.setItemMeta(IronChestplateMeta);
						player.getInventory().setChestplate(IronChestplate);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1250) {
						if(!Purchases.contains("IronChestplate")) {
							Purchases.add("IronChestplate");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Iron Chestplate");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1250));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Iron Leggings")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("IronLeggings")) {
						ItemStack IronLeggings = new ItemStack(Material.IRON_LEGGINGS);
						ItemMeta IronLeggingsMeta = IronLeggings.getItemMeta();
						IronLeggings.setItemMeta(IronLeggingsMeta);
						player.getInventory().setLeggings(IronLeggings);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 1000) {
						if(!Purchases.contains("IronLeggings")) {
							Purchases.add("IronLeggings");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Iron Leggings");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 1000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Iron Boots")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("IronBoots")) {
						ItemStack IronBoots = new ItemStack(Material.IRON_BOOTS);
						ItemMeta IronBootsMeta = IronBoots.getItemMeta();
						IronBoots.setItemMeta(IronBootsMeta);
						player.getInventory().setBoots(IronBoots);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("IronBoots")) {
							Purchases.add("IronBoots");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Iron Boots");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold Helmet")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack GoldHelmet = new ItemStack(Material.GOLD_HELMET);
						ItemMeta GoldHelmetMeta = GoldHelmet.getItemMeta();
						GoldHelmet.setItemMeta(GoldHelmetMeta);
						player.getInventory().setHelmet(GoldHelmet);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold Chestplate")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack GoldChestplate = new ItemStack(Material.GOLD_CHESTPLATE);
						ItemMeta GoldChestplateMeta = GoldChestplate.getItemMeta();
						GoldChestplate.setItemMeta(GoldChestplateMeta);
						player.getInventory().setChestplate(GoldChestplate);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold Leggings")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack GoldLeggings = new ItemStack(Material.GOLD_LEGGINGS);
						ItemMeta GoldLeggingsMeta = GoldLeggings.getItemMeta();
						GoldLeggings.setItemMeta(GoldLeggingsMeta);
						player.getInventory().setLeggings(GoldLeggings);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Gold Boots")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack GoldBoots = new ItemStack(Material.GOLD_BOOTS);
						ItemMeta GoldBootsMeta = GoldBoots.getItemMeta();
						GoldBoots.setItemMeta(GoldBootsMeta);
						player.getInventory().setBoots(GoldBoots);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Diamond Helmet")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack DiamondHelmet = new ItemStack(Material.DIAMOND_HELMET);
						ItemMeta DiamondHelmetMeta = DiamondHelmet.getItemMeta();
						DiamondHelmet.setItemMeta(DiamondHelmetMeta);
						player.getInventory().setHelmet(DiamondHelmet);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Diamond Chestplate")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack DiamondChestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
						ItemMeta DiamondChestplateMeta = DiamondChestplate.getItemMeta();
						DiamondChestplate.setItemMeta(DiamondChestplateMeta);
						player.getInventory().setChestplate(DiamondChestplate);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Diamond Leggings")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack DiamondLeggings = new ItemStack(Material.DIAMOND_LEGGINGS);
						ItemMeta DiamondLeggingsMeta = DiamondLeggings.getItemMeta();
						DiamondLeggings.setItemMeta(DiamondLeggingsMeta);
						player.getInventory().setLeggings(DiamondLeggings);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Diamond Boots")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						ItemStack DiamondBoots = new ItemStack(Material.DIAMOND_BOOTS);
						ItemMeta DiamondBootsMeta = DiamondBoots.getItemMeta();
						DiamondBoots.setItemMeta(DiamondBootsMeta);
						player.getInventory().setBoots(DiamondBoots);
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}
		}
	}
	@EventHandler
	public void PlayerWardrobeJoinSoundsInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		List<String> Purchases = this.plugin.electrodatabase.getStringList("Players." + player.getUniqueId() + ".Purchased");
		if(!e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - J Sounds")) return;
		if(e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe - J Sounds")) {
			e.setCancelled(true);
			if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Reset Join Sound")) {
				this.plugin.getConfig().set("Player." + player.getUniqueId() + ".JoinSound", "None");
				Location loc = player.getLocation();
				player.playSound(loc, Sound.EXPLODE, 1, 3);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Main Menu")) {
				PlayerWardrobeGUI(player);
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Water Dunk Sound")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("WaterDunkJoinSound")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".JoinSound", "WaterDunk");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("WaterDunkJoinSound")) {
							Purchases.add("WaterDunkJoinSound");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Water Dunk Join Sound");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Anvil Drop Sound")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("AnvilDropJoinSound")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".JoinSound", "AnvilDrop");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("AnvilDropJoinSound")) {
							Purchases.add("AnvilDropJoinSound");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Anvil Drop Join Sound");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Piston Extending Sound")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("PistonExtendingJoinSound")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".JoinSound", "PistonExtending");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("PistonExtendingJoinSound")) {
							Purchases.add("PistonExtendingJoinSound");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Piston Extending Join Sound");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Cat Meow Sound")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("CatMeowJoinSound")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".JoinSound", "CatMeow");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2500) {
						if(!Purchases.contains("CatMeowJoinSound")) {
							Purchases.add("CatMeowJoinSound");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Cat Meow Join Sound");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Exploding TNT Sound")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".JoinSound", "ExplodingTNT");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Chest Opening Sound")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("ChestOpeningJoinSound")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".JoinSound", "ChestOpening");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2000) {
						if(!Purchases.contains("ChestOpeningJoinSound")) {
							Purchases.add("ChestOpeningJoinSound");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Chest Opening Join Sound");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Ghast Death Sound")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("GhastDeathJoinSound")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".JoinSound", "GhastDeath");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 3000) {
						if(!Purchases.contains("GhastDeathJoinSound")) {
							Purchases.add("GhastDeathJoinSound");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Ghast Death Join Sound");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 3000));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Lightning Sound")) {
				if(player.hasPermission("electronetworks.titan") || player.hasPermission("electronetworks.lord") || player.hasPermission("electronetworks.master")) {
					if(e.isLeftClick() == true) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".JoinSound", "Lightning");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You cannot equip this as it is a donator only cosmetic.");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Dog Bark Sound")) {
				if(e.isLeftClick() == true) {
					if(Purchases.contains("DogBarkJoinSound")) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".JoinSound", "DogBark");
						this.plugin.saveConfig();
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else{
						player.sendMessage(prefix + ChatColor.RED + "You must buy this cosmetic before you can use it!");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
					}
				}else if(e.isRightClick() == true) {
					if(this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balanace") >= 2500) {
						if(!Purchases.contains("DogBarkJoinSound")) {
							Purchases.add("DogBarkJoinSound");
							this.plugin.electrodatabase.set("Players." + player.getUniqueId() + ".Purchased", Purchases);
							this.plugin.saveelectrodatabase();
							player.sendMessage(prefix + ChatColor.YELLOW + "You successfully bought: " + ChatColor.GOLD + "Dog Bark Join Sound");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.NOTE_PLING, 1, 3);
							this.plugin.getConfig().set("Electros." + player.getUniqueId() + ".Balance", (this.plugin.getConfig().getInt("Electros." + player.getUniqueId() + ".Balance") - 2500));
							this.plugin.saveConfig();
						}else{
							player.sendMessage(prefix + ChatColor.RED + "You have already bought this cosmetic!");
							Location loc = player.getLocation();
							player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
						}
					}else{
						player.sendMessage(prefix + ChatColor.RED + "Sorry, you do not have enough electros to buy this cosmetic.");
					}
				}
			}
		}
	}
}