package me.Dylan_H.ElectroNetworksHub;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class OnJoin implements Listener{

	Main plugin;
	
	public OnJoin(Main instance2) {
		this.plugin = instance2;
	}
	@EventHandler
	public void PlayerJoinMessages(PlayerJoinEvent e) {
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		Player player = e.getPlayer();
		player.sendMessage(prefix + ChatColor.YELLOW + "You connected to the server: " + ChatColor.GOLD + this.plugin.getConfig().getString("PlayerJoinMessages.Server"));
		player.sendMessage(prefix + ChatColor.YELLOW + "News: " + ChatColor.GOLD + this.plugin.getConfig().getString("PlayerJoinMessages.News"));
		if(this.plugin.getConfig().getString("ServerMode").equals("Development_Mode")) {
			player.sendMessage(prefix + ChatColor.BLUE + "" + ChatColor.BOLD + "Development Mode is active! Please expect bugs and glitches.");
		}else if(this.plugin.getConfig().getString("ServerMode").equals("Build_Mode")) {
			player.sendMessage(prefix + ChatColor.BLUE + "" + ChatColor.BOLD + "Build Mode is active! Please expect build exploits.");
		}
	}
	@EventHandler
	public void PlayerJoinAnnouncements(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		if(player.hasPlayedBefore() == false) {
			e.setJoinMessage(prefix + ChatColor.YELLOW + "Please welcome, " + ChatColor.GOLD + player.getName() + ChatColor.YELLOW + ", to the " + ChatColor.GOLD + "Electro Networks " + ChatColor.YELLOW + "community!");
		}else if(player.hasPlayedBefore() ==  true) {
			if(player.hasPermission("electronetworks.staff")) {
				e.setJoinMessage(prefix + ChatColor.GOLD + "Staff Member: " + player.getName() + ChatColor.YELLOW + " has joined the game.");
			}else{
				e.setJoinMessage(prefix + ChatColor.GRAY + player.getName() + " has joined the game.");
			}
		}
	}
	@EventHandler
	public void PlayerNameTags(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		if(player.hasPermission("electronetworks.staff")) {
			player.setPlayerListName(ChatColor.GRAY + "[" + ChatColor.GOLD + "" + ChatColor.BOLD + "Staff" + ChatColor.RESET + ChatColor.GRAY + "] " + player.getName());
		}else if(player.hasPermission("electronetworks.titan")) {
			player.setPlayerListName(ChatColor.GRAY + "[" + ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Titan" + ChatColor.RESET + ChatColor.GRAY + "] " + player.getName());
		}else if(player.hasPermission("electronetworks.lord")) {
			player.setPlayerListName(ChatColor.GRAY + "[" + ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Lord" + ChatColor.RESET + ChatColor.GRAY + "] " + player.getName());
		}else if(player.hasPermission("electronetworks.master")) {
			player.setPlayerListName(ChatColor.GRAY + "[" + ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Master" + ChatColor.RESET + ChatColor.GRAY + "] " + player.getName());
		}else{
			player.setPlayerListName(ChatColor.GRAY + player.getName());
		}
	}
	@EventHandler
	public void PlayerJoinItems(PlayerJoinEvent e) {
		e.getPlayer().getInventory().clear();
		// Server Selector
		Player player = e.getPlayer();
		ItemStack ServerSelector = new ItemStack(Material.COMPASS);
		ItemMeta ServerSelectorMeta = ServerSelector.getItemMeta();
		ServerSelectorMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Servers");
		ServerSelectorMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Right-click to select one of",ChatColor.GOLD + "  our servers to play on."));
		ServerSelector.setItemMeta(ServerSelectorMeta);
		player.getInventory().setItem(0, ServerSelector);
		// Rule Book
		ItemStack RuleBook = new ItemStack(Material.WRITTEN_BOOK, 1);
		BookMeta RuleBookMeta = (BookMeta)RuleBook.getItemMeta();
		RuleBookMeta.setTitle(ChatColor.YELLOW + "" + ChatColor.BOLD + "Rules");
		RuleBookMeta.setAuthor(ChatColor.YELLOW + "Electro Networks");
		RuleBookMeta.addPage(
				ChatColor.BLACK + "-======-\n" + ChatColor.GOLD + "Welcome\n" + ChatColor.BLACK + "-======-\nWelcome to Electro Networks! Here you can find many unique gamemodes. Before playing our server, we suggest you have a look at our rules on the next page.");
		RuleBookMeta.addPage(
				ChatColor.BLACK + "-======-\n" + ChatColor.GOLD + "Rules\n" + ChatColor.BLACK + "-======-\nOn this page, you'll find the rules of our server.\n" + ChatColor.GOLD + "1 - No advertising.\n2 - No Swearing.\n3 - Respect all players and staff.\n4 - Don't exploit bugs and glitches.\n5 - No spamming.\n6 - No hacked clients.");
		RuleBookMeta.addPage(
				ChatColor.BLUE + "We have rules because we want our server to be a fun place to play.\n" + ChatColor.RED + "Failure to comply with these rules will result in a punishment. Donating does not exempt you from the rules!");
		RuleBook.setItemMeta(RuleBookMeta);
		player.getInventory().setItem(1, RuleBook);
		// Gadgets
		ItemStack Music = new ItemStack(Material.BLAZE_ROD);
		ItemMeta MusicMeta = Music.getItemMeta();
		MusicMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Firework Thrower");
		MusicMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Right-click to throw a firework."));
		Music.setItemMeta(MusicMeta);
		player.getInventory().setItem(4, Music);
		// Player Settings
		ItemStack PlayerSettings = new ItemStack(Material.REDSTONE_COMPARATOR);
		ItemMeta PlayerSettingsMeta = PlayerSettings.getItemMeta();
		PlayerSettingsMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Settings");
		PlayerSettingsMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Right-click to select what you",ChatColor.GOLD + "  want enabled/disabled."));
		PlayerSettings.setItemMeta(PlayerSettingsMeta);
		player.getInventory().setItem(8, PlayerSettings);
		// Player Wardrobe
		ItemStack PlayerWardrobe = new ItemStack(Material.BOOKSHELF);
		ItemMeta PlayerWardrobeMeta = PlayerWardrobe.getItemMeta();
		PlayerWardrobeMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe");
		PlayerWardrobeMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Right-click to select what you",ChatColor.GOLD + "  you want to wear and more."));
		PlayerWardrobe.setItemMeta(PlayerWardrobeMeta);
		player.getInventory().setItem(7, PlayerWardrobe);
	}
	@EventHandler
	public void SpawnTeleport(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		if(player.hasPermission("electronetworks.spawn")) {
			Location Spawn = new Location(player.getWorld(), this.plugin.getConfig().getInt("SpawnPoint.X"), this.plugin.getConfig().getInt("SpawnPoint.Y"), this.plugin.getConfig().getInt("SpawnPoint.Z"));
			player.teleport(Spawn);
		}
	}
	// Player Settings
	@EventHandler
	public void PlayerSettings(PlayerJoinEvent e) {
		if(this.plugin.getConfig().getBoolean("Players." + e.getPlayer().getUniqueId() + ".Speed") == true) {
			e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999999, 3));
		}if(this.plugin.getConfig().getBoolean("Players." + e.getPlayer().getUniqueId() + ".Jump") == true) {
			e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 999999999, 2));
		}if(this.plugin.getConfig().getBoolean("Players." + e.getPlayer().getUniqueId() + ".NightVision") == true) {
			e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 999999999, 0));
		}if(this.plugin.getConfig().getBoolean("Players." + e.getPlayer().getUniqueId() + ".Flight") == true) {
			e.getPlayer().setAllowFlight(true);
			e.getPlayer().setFlying(true);
		}
	}
	// Player Join Sounds
	@EventHandler
	public void PlayerJoinSounds(PlayerJoinEvent e) {
		if(this.plugin.getConfig().getString("Players." + e.getPlayer().getUniqueId() + ".JoinSound").equalsIgnoreCase("WaterDunk")) {
			for(Player allPlayers : Bukkit.getServer().getOnlinePlayers()) {
				Location loc = allPlayers.getLocation();
				allPlayers.playSound(loc, Sound.SPLASH, 1, 0);
			}
		}else if(this.plugin.getConfig().getString("Players." + e.getPlayer().getUniqueId() + ".JoinSound").equalsIgnoreCase("AnvilDrop")) {
			for(Player allPlayers : Bukkit.getServer().getOnlinePlayers()) {
				Location loc = allPlayers.getLocation();
				allPlayers.playSound(loc, Sound.ANVIL_LAND, 1, 0);
			}
		}else if(this.plugin.getConfig().getString("Players." + e.getPlayer().getUniqueId() + ".JoinSound").equalsIgnoreCase("PistonExtending")) {
			for(Player allPlayers : Bukkit.getServer().getOnlinePlayers()) {
				Location loc = allPlayers.getLocation();
				allPlayers.playSound(loc, Sound.PISTON_EXTEND, 1, 0);
			}
		}else if(this.plugin.getConfig().getString("Players." + e.getPlayer().getUniqueId() + ".JoinSound").equalsIgnoreCase("CatMeow")) {
			for(Player allPlayers : Bukkit.getServer().getOnlinePlayers()) {
				Location loc = allPlayers.getLocation();
				allPlayers.playSound(loc, Sound.CAT_MEOW, 1, 0);
			}
		}else if(this.plugin.getConfig().getString("Players." + e.getPlayer().getUniqueId() + ".JoinSound").equalsIgnoreCase("ExplodingTNT")) {
			for(Player allPlayers : Bukkit.getServer().getOnlinePlayers()) {
				Location loc = allPlayers.getLocation();
				allPlayers.playSound(loc, Sound.EXPLODE, 1, 0);
			}
		}else if(this.plugin.getConfig().getString("Players." + e.getPlayer().getUniqueId() + ".JoinSound").equalsIgnoreCase("ChestOpening")) {
			for(Player allPlayers : Bukkit.getServer().getOnlinePlayers()) {
				Location loc = allPlayers.getLocation();
				allPlayers.playSound(loc, Sound.CHEST_OPEN, 1, 0);
			}
		}else if(this.plugin.getConfig().getString("Players." + e.getPlayer().getUniqueId() + ".JoinSound").equalsIgnoreCase("GhastDeath")) {
			for(Player allPlayers : Bukkit.getServer().getOnlinePlayers()) {
				Location loc = allPlayers.getLocation();
				allPlayers.playSound(loc, Sound.GHAST_DEATH, 1, 0);
			}
		}else if(this.plugin.getConfig().getString("Players." + e.getPlayer().getUniqueId() + ".JoinSound").equalsIgnoreCase("Lightning")) {
			for(Player allPlayers : Bukkit.getServer().getOnlinePlayers()) {
				Location loc = allPlayers.getLocation();
				allPlayers.playSound(loc, Sound.AMBIENCE_THUNDER, 1, 0);
			}
		}else if(this.plugin.getConfig().getString("Players." + e.getPlayer().getUniqueId() + ".JoinSound").equalsIgnoreCase("DogBark")) {
			for(Player allPlayers : Bukkit.getServer().getOnlinePlayers()) {
				Location loc = allPlayers.getLocation();
				allPlayers.playSound(loc, Sound.WOLF_BARK, 1, 0);
			}
		}
	}
}