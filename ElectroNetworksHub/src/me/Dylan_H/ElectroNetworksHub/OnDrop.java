package me.Dylan_H.ElectroNetworksHub;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class OnDrop implements Listener{

	Main plugin;
	
	public OnDrop(Main instance9) {
		this.plugin = instance9;
	}
	@EventHandler
	public void ItemDrop(PlayerDropItemEvent e) {
		Item DroppedItem = e.getItemDrop();
		if(DroppedItem.getItemStack() != null) {
			if(DroppedItem.getItemStack().getItemMeta() != null) {
				if(DroppedItem.getItemStack().getItemMeta().getDisplayName() != null) {
					if(DroppedItem.getItemStack().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Servers")) {
						e.setCancelled(true);
					}else if(DroppedItem.getItemStack().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Firework Thrower")) {
						e.setCancelled(true);
					}else if(DroppedItem.getItemStack().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Wardrobe")) {
						e.setCancelled(true);
					}else if(DroppedItem.getItemStack().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Settings")) {
						e.setCancelled(true);
					}
				}else if(DroppedItem.getItemStack().getType().equals(Material.WRITTEN_BOOK)) {
					e.setCancelled(true);
				}
			}
		}
	}
}