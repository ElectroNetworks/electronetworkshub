package me.Dylan_H.ElectroNetworksHub;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerSettings implements Listener {

	Main plugin;
	
	public PlayerSettings(Main instance5) {
		this.plugin = instance5;
	}
	public void PlayerSettingsGUI(Player player) {
		Inventory PlayerSettingsGUI = Bukkit.createInventory(null, 36, ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Settings");
		// Items
		ItemStack Speed = new ItemStack(Material.LEATHER_BOOTS);
		ItemMeta SpeedMeta = Speed.getItemMeta();
		
		ItemStack Jump = new ItemStack(Material.FEATHER);
		ItemMeta JumpMeta = Jump.getItemMeta();
		
		ItemStack Chat = new ItemStack(Material.BOOK);
		ItemMeta ChatMeta = Chat.getItemMeta();
		
		ItemStack NightVision = new ItemStack(Material.EYE_OF_ENDER);
		ItemMeta NightVisionMeta = NightVision.getItemMeta();
		
		ItemStack Fly = new ItemStack(Material.FIREWORK);
		ItemMeta FlyMeta = Fly.getItemMeta();
		// Togglers (Disabled)
		ItemStack SpeedToggleDisabled = new ItemStack(Material.INK_SACK, 1, (byte) 8);
		ItemMeta SpeedToggleDisabledMeta = SpeedToggleDisabled.getItemMeta();
		
		ItemStack JumpToggleDisabled = new ItemStack(Material.INK_SACK, 1, (byte) 8);
		ItemMeta JumpToggleDisabledMeta = JumpToggleDisabled.getItemMeta();
		
		ItemStack ChatToggleDisabled = new ItemStack(Material.INK_SACK, 1, (byte) 8);
		ItemMeta ChatToggleDisabledMeta = ChatToggleDisabled.getItemMeta();
		
		ItemStack NightVisionToggleDisabled = new ItemStack(Material.INK_SACK, 1, (byte) 8);
		ItemMeta NightVisionToggleDisabledMeta = NightVisionToggleDisabled.getItemMeta();
		
		ItemStack FlyToggleDisabled = new ItemStack(Material.INK_SACK, 1, (byte) 8);
		ItemMeta FlyToggleDisabledMeta = FlyToggleDisabled.getItemMeta();
		// Togglers (Enabled)
		ItemStack SpeedToggleEnabled = new ItemStack(Material.INK_SACK, 1, (byte) 10);
		ItemMeta SpeedToggleEnabledMeta = SpeedToggleEnabled.getItemMeta();
		
		ItemStack JumpToggleEnabled = new ItemStack(Material.INK_SACK, 1, (byte) 10);
		ItemMeta JumpToggleEnabledMeta = JumpToggleEnabled.getItemMeta();
		
		ItemStack ChatToggleEnabled = new ItemStack(Material.INK_SACK, 1, (byte) 10);
		ItemMeta ChatToggleEnabledMeta = ChatToggleEnabled.getItemMeta();
		
		ItemStack NightVisionToggleEnabled = new ItemStack(Material.INK_SACK, 1, (byte) 10);
		ItemMeta NightVisionToggleEnabledMeta = NightVisionToggleEnabled.getItemMeta();
		
		ItemStack FlyToggleEnabled = new ItemStack(Material.INK_SACK, 1, (byte) 10);
		ItemMeta FlyToggleEnabledMeta = FlyToggleEnabled.getItemMeta();
		// Display Names (Items)
		SpeedMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Speed Boost");
		SpeedMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to toggle your speed boost."));
		Speed.setItemMeta(SpeedMeta);
		
		JumpMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Jump Boost");
		JumpMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to toggle your jump boost."));
		Jump.setItemMeta(JumpMeta);
		
		ChatMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Chat Visibility");
		ChatMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to toggle your chat visibility."));
		Chat.setItemMeta(ChatMeta);
		
		NightVisionMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Night Vision");
		NightVisionMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to toggle your night vision effect."));
		NightVision.setItemMeta(NightVisionMeta);
		
		FlyMeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Flight");
		FlyMeta.setLore(Arrays.asList(ChatColor.GOLD + "▶ Click here to toggle your flight mode."));
		Fly.setItemMeta(FlyMeta);
		// Display Names (Togglers) (Disabled)
		SpeedToggleDisabledMeta.setDisplayName(ChatColor.RED + "Disabled");
		SpeedToggleDisabled.setItemMeta(SpeedToggleDisabledMeta);
		
		JumpToggleDisabledMeta.setDisplayName(ChatColor.RED + "Disabled");
		JumpToggleDisabled.setItemMeta(JumpToggleDisabledMeta);
		
		ChatToggleDisabledMeta.setDisplayName(ChatColor.RED + "Disabled");
		ChatToggleDisabled.setItemMeta(ChatToggleDisabledMeta);
		
		NightVisionToggleDisabledMeta.setDisplayName(ChatColor.RED + "Disabled");
		NightVisionToggleDisabled.setItemMeta(NightVisionToggleDisabledMeta);
		
		FlyToggleDisabledMeta.setDisplayName(ChatColor.RED + "Disabled");
		FlyToggleDisabled.setItemMeta(FlyToggleDisabledMeta);
		// Display Names (Togglers) (Enabled)
		SpeedToggleEnabledMeta.setDisplayName(ChatColor.GREEN + "Enabled");
		SpeedToggleEnabled.setItemMeta(SpeedToggleEnabledMeta);
		
		JumpToggleEnabledMeta.setDisplayName(ChatColor.GREEN + "Enabled");
		JumpToggleEnabled.setItemMeta(JumpToggleEnabledMeta);
		
		ChatToggleEnabledMeta.setDisplayName(ChatColor.GREEN + "Enabled");
		ChatToggleEnabled.setItemMeta(ChatToggleEnabledMeta);
		
		NightVisionToggleEnabledMeta.setDisplayName(ChatColor.GREEN + "Enabled");
		NightVisionToggleEnabled.setItemMeta(NightVisionToggleEnabledMeta);
		
		FlyToggleEnabledMeta.setDisplayName(ChatColor.GREEN + "Enabled");
		FlyToggleEnabled.setItemMeta(FlyToggleEnabledMeta);
		
		// Setting The Items
		PlayerSettingsGUI.setItem(9, Speed);
		PlayerSettingsGUI.setItem(11, Jump);
		PlayerSettingsGUI.setItem(13, Chat);
		PlayerSettingsGUI.setItem(15, NightVision);
		PlayerSettingsGUI.setItem(17, Fly);
		
		if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Speed") == false) {
			PlayerSettingsGUI.setItem(18, SpeedToggleDisabled);
		}else if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Speed") == true) {
			PlayerSettingsGUI.setItem(18, SpeedToggleEnabled);
		}
		if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Jump") == false) {
			PlayerSettingsGUI.setItem(20, JumpToggleDisabled);
		}else if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Jump") == true) {
			PlayerSettingsGUI.setItem(20, JumpToggleEnabled);
		}
		if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Chat") == false) {
			PlayerSettingsGUI.setItem(22, ChatToggleDisabled);
		}else if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Chat") == true) {
			PlayerSettingsGUI.setItem(22, ChatToggleEnabled);
		}
		if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".NightVision") == false) {
			PlayerSettingsGUI.setItem(24, NightVisionToggleDisabled);
		}else if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".NightVision") == true) {
			PlayerSettingsGUI.setItem(24, NightVisionToggleEnabled);
		}
		if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Flight") == false) {
			PlayerSettingsGUI.setItem(26, FlyToggleDisabled);
		}else if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Flight") == true) {
			PlayerSettingsGUI.setItem(26, FlyToggleEnabled);
		}
		
		player.openInventory(PlayerSettingsGUI);
	}
	@EventHandler
	public void PlayerSettingsInteract(PlayerInteractEvent e) {
		if((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
			ItemStack currentItem = e.getPlayer().getItemInHand();
			if((currentItem == null) || (currentItem.getType() == Material.AIR)) {
				return;
			}
			if((!currentItem.hasItemMeta()) || ((currentItem.hasItemMeta()) && (!currentItem.getItemMeta().hasDisplayName()))) {
				return;
			}
			if(currentItem.getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Settings")) {
				Player player = (Player) e.getPlayer();
				PlayerSettingsGUI(player);
			}
		}
	}
	@EventHandler
	public void PlayerSettingsInventoryClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		String prefix = ChatColor.YELLOW + "Electro " + ChatColor.GRAY + "|| ";
		if(!e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Settings")) return;
		if(e.getInventory().getName().equalsIgnoreCase(ChatColor.YELLOW + "" + ChatColor.BOLD + "Player Settings")) {
			e.setCancelled(true);
			if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Speed Boost")) {
				if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Speed") == false) {
					this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Speed", true);
					PlayerSettingsGUI(player);
					player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999999, 3));
					player.sendMessage(prefix + ChatColor.YELLOW + "You enabled the setting: " + ChatColor.GOLD + "Speed Boost");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.NOTE_PLING, 1, 3);
				}else if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Speed") ==  true) {
					this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Speed", false);
					PlayerSettingsGUI(player);
					player.removePotionEffect(PotionEffectType.SPEED);
					player.sendMessage(prefix + ChatColor.YELLOW + "You disabled the setting: " + ChatColor.GOLD + "Speed Boost");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.NOTE_PLING, 1, 3);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Jump Boost")) {
				if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Jump") == false) {
					this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Jump", true);
					PlayerSettingsGUI(player);
					player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 999999999, 2));
					player.sendMessage(prefix + ChatColor.YELLOW + "You enabled the setting: " + ChatColor.GOLD + "Jump Boost");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.NOTE_PLING, 1, 3);
				}else if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Jump") ==  true) {
					this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Jump", false);
					PlayerSettingsGUI(player);
					player.removePotionEffect(PotionEffectType.JUMP);
					player.sendMessage(prefix + ChatColor.YELLOW + "You disabled the setting: " + ChatColor.GOLD + "Jump Boost");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.NOTE_PLING, 1, 3);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Chat Visibility")) {
				// Temporary Code
				PlayerSettingsGUI(player);
				player.sendMessage(prefix + ChatColor.RED + "Sorry, this setting is currently in development.");
				Location loc = player.getLocation();
				player.playSound(loc, Sound.ITEM_BREAK, 1, 0);
				// Actual Code
				//if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Chat") == false) {
					//this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Chat", true);
					//PlayerSettingsGUI(player);
					// TURN ON CHAT HERE
					//player.sendMessage(prefix + ChatColor.YELLOW + "You enabled the setting: " + ChatColor.GOLD + "Chat Visibility");
				//}else if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Chat") == true) {
					//this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Chat", false);
					//PlayerSettingsGUI(player);
					// TURN OFF CHAT HERE
					//player.sendMessage(prefix + ChatColor.YELLOW + "You disabled the setting: " + ChatColor.GOLD + "Chat Visibility");
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Night Vision")) {
				if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".NightVision") == false) {
					this.plugin.getConfig().set("Players." + player.getUniqueId() + ".NightVision", true);
					PlayerSettingsGUI(player);
					player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 999999999, 0));
					player.sendMessage(prefix + ChatColor.YELLOW + "You enabled the setting: " + ChatColor.GOLD + "Night Vision");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.NOTE_PLING, 1, 3);
				}else if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".NightVision") ==  true) {
					this.plugin.getConfig().set("Players." + player.getUniqueId() + ".NightVision", false);
					PlayerSettingsGUI(player);
					player.removePotionEffect(PotionEffectType.NIGHT_VISION);
					player.sendMessage(prefix + ChatColor.YELLOW + "You disabled the setting: " + ChatColor.GOLD + "Night Vision");
					Location loc = player.getLocation();
					player.playSound(loc, Sound.NOTE_PLING, 1, 3);
				}
			}else if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "" + ChatColor.BOLD + "Flight")) {
				if(player.hasPermission("electronetworks.fly")) {
					if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Flight") == false) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Flight", true);
						PlayerSettingsGUI(player);
						player.setAllowFlight(true);
						player.sendMessage(prefix + ChatColor.YELLOW + "You enabled the setting: " + ChatColor.GOLD + "Flight");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}else if(this.plugin.getConfig().getBoolean("Players." + player.getUniqueId() + ".Flight") ==  true) {
						this.plugin.getConfig().set("Players." + player.getUniqueId() + ".Flight", false);
						PlayerSettingsGUI(player);
						player.setAllowFlight(false);
						player.sendMessage(prefix + ChatColor.YELLOW + "You disabled the setting: " + ChatColor.GOLD + "Flight");
						Location loc = player.getLocation();
						player.playSound(loc, Sound.NOTE_PLING, 1, 3);
					}
				}else{
					player.sendMessage(prefix + ChatColor.RED + "You do not have permission to toggle: " + ChatColor.GOLD + "Flight");
				}
			}
		}
	}
}